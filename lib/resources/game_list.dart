import 'package:quinb/models/game_category.dart';
import 'package:quinb/models/game_detail.dart';
import 'package:quinb/models/game_mode.dart';
import 'package:quinb/i18n/game_list.i18n.dart';

// List of games
List<GameDetails> get gameList => [
      GameDetails(
        title: 'Remember'.i18n,
        subtitle: 'Remember the colors and letters that appear'.i18n,
        mode: GameMode.remember,
        category: GameCategory.Logic,
        rules:
            'Remember the sequence of colors and letters that will appear on the screen'
                .i18n,
      ),
      GameDetails(
        title: 'Three figures'.i18n,
        subtitle: 'Find three identical figures'.i18n,
        inGameTip: 'Tap when there are 3 identical figures'.i18n,
        mode: GameMode.threeFigures,
        category: GameCategory.Logic,
        rules:
            'Tap the screen when you see at least 3 figures that have the same color and the same letter inside'
                .i18n,
      ),
      GameDetails(
        title: 'Vibration repetitions'.i18n,
        subtitle: 'Count how many times you hear the vibration'.i18n,
        inGameTip: 'How many times have you heard the vibration?'.i18n,
        mode: GameMode.vibrationCount,
        category: GameCategory.Vibration,
        rules: 'Count how many times you hear the vibration'.i18n,
      ),
      GameDetails(
        title: 'Count the lights'.i18n,
        subtitle: 'Count how many lights come on'.i18n,
        inGameTip: 'How many light have come on?'.i18n,
        mode: GameMode.lightCount,
        category: GameCategory.Logic,
        rules:
            'Some lights are about to come on in rapid succession.\n\nCounts how many lights come on.'
                .i18n,
      ),
      GameDetails(
        title: 'Sound repetitions'.i18n,
        subtitle: 'Count how many times you hear the sound'.i18n,
        inGameTip: 'How many times have you heard the sound?'.i18n,
        mode: GameMode.soundCount,
        category: GameCategory.Sound,
        rules: 'Count how many times you hear the sound'.i18n,
      ),
      GameDetails(
        title: 'Shortest vibration'.i18n,
        subtitle: 'Find the shortest vibration'.i18n,
        inGameTip: 'Which one was the shortest vibration?'.i18n,
        mode: GameMode.shortestVibration,
        category: GameCategory.Vibration,
        rules:
            'Try to figure out which one is the shortest vibration among four'
                .i18n,
      ),
      GameDetails(
        title: 'Math calculation'.i18n,
        subtitle: 'Solve the equations'.i18n,
        mode: GameMode.mathCalculation,
        category: GameCategory.Logic,
        rules: 'Solve the equations in your head as fast as you can'.i18n,
      ),
      GameDetails(
        title: 'Lights on'.i18n,
        subtitle: 'Count if there are more lights on than off'.i18n,
        inGameTip: 'Are there more lights on than off?'.i18n,
        mode: GameMode.lightsOn,
        category: GameCategory.Logic,
        rules: 'Tap the screen if there are more yellow squares than black ones'
            .i18n,
      ),
      GameDetails(
        title: 'Vibration sequence'.i18n,
        subtitle: 'Remember the sequence you hear'.i18n,
        inGameTip: 'Which sequence was it?'.i18n,
        mode: GameMode.sequenceVibration,
        category: GameCategory.Vibration,
        rules:
            'Listen to the vibration sequence and remember it.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration'
                .i18n,
      ),
      GameDetails(
        title: 'Mix Colors'.i18n,
        subtitle: 'Mix the colors'.i18n,
        mode: GameMode.colorCalculation,
        category: GameCategory.Logic,
        rules:
            'Try to figure out which color results from the color mixing'.i18n,
      ),
      GameDetails(
        title: 'Audio sequence'.i18n,
        subtitle: 'Remember the sequence you hear'.i18n,
        inGameTip: 'Which sequence was it?'.i18n,
        mode: GameMode.sequenceSound,
        category: GameCategory.Sound,
        rules: 'Listen to the sounds sequence and remember it'.i18n,
      ),
      GameDetails(
        title: 'Reflexes'.i18n,
        subtitle: 'Tap at the right moment'.i18n,
        mode: GameMode.reflexes,
        category: GameCategory.Logic,
        rules:
            'As soon as the screen becomes colored, tap the option of the same color.\n\nKeep in mind that you have little time before the screen turns white again.'
                .i18n,
      ),
      GameDetails(
        title: 'Fastest object'.i18n,
        subtitle: 'Find the fastest object'.i18n,
        mode: GameMode.objectSpeed,
        category: GameCategory.Logic,
        rules: 'Try to figure out which one is the fastest moving object'.i18n,
      ),
      GameDetails(
        title: 'What color is it'.i18n,
        subtitle: 'Recognize which color is used'.i18n,
        mode: GameMode.colorUsed,
        category: GameCategory.Logic,
        rules:
            'Answer as fast as you can which color is written or which color is used for the background or for the text'
                .i18n,
      ),
      GameDetails(
        title: 'Listen and calculate'.i18n,
        subtitle: 'Add or subtract based on what you hear'.i18n,
        inGameTip: 'What is the result?'.i18n,
        mode: GameMode.listenMath,
        category: GameCategory.Sound,
        rules:
            'Sum if you hear a duck, subtract if you hear a cat.\n\nThe amount of the sum and the subtraction will be shown during the game.'
                .i18n,
      ),
      GameDetails(
        title: 'Recognize the sound'.i18n,
        subtitle: 'Tap the screen when you hear the sound'.i18n,
        mode: GameMode.recognize,
        category: GameCategory.Sound,
        rules:
            'Listen to the sample sound twice here.\n\nThen tap the screen when you hear it during a sequence of sounds.'
                .i18n,
      ),
      GameDetails(
        title: 'Hear and calculate'.i18n,
        subtitle: 'Add or subtract based on vibrations'.i18n,
        inGameTip: 'What is the result?'.i18n,
        mode: GameMode.vibrationMath,
        category: GameCategory.Vibration,
        rules:
            'Sum if you hear a long vibration, subtract if you hear a short one.\n\nThe amount of the sum and the subtraction will be shown during the game.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration'
                .i18n,
      ),
      GameDetails(
        title: 'Animal farm'.i18n,
        subtitle: 'Tap when you hear the correct animal'.i18n,
        mode: GameMode.farm,
        category: GameCategory.Sound,
        rules:
            'Then tap the screen when you hear the correct animal during a sequence of sounds'
                .i18n,
      ),
    ];
