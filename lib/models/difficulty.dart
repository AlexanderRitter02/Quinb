import 'package:flutter/foundation.dart';

import 'package:quinb/i18n/difficulty.i18n.dart';

// Game difficulty
enum Difficulty { Easiest, Easy, Medium, Hard, Hardest }

extension DifficultyName on Difficulty {
  // i18n: 'Easiest'.i18n 'Easy'.i18n 'Medium'.i18n 'Hard'.i18n 'Hardest'.i18n
  String get name => describeEnum(this).i18n;
}
