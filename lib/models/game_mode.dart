import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/games/calculation.dart';
import 'package:quinb/games/color_used.dart';
import 'package:quinb/games/count.dart';
import 'package:quinb/games/farm.dart';
import 'package:quinb/games/light_count.dart';
import 'package:quinb/games/lights_on.dart';
import 'package:quinb/games/listen_math.dart';
import 'package:quinb/games/object_speed.dart';
import 'package:quinb/games/recognize.dart';
import 'package:quinb/games/reflexes.dart';
import 'package:quinb/games/remember.dart';
import 'package:quinb/games/sequence.dart';
import 'package:quinb/games/shortest_vibration.dart';
import 'package:quinb/games/three_figures.dart';
import 'package:quinb/ui/screens/game/game_page.dart';

// Game modes
enum GameMode {
  colorCalculation,
  colorUsed,
  farm,
  lightCount,
  lightsOn,
  listenMath,
  mathCalculation,
  objectSpeed,
  recognize,
  reflexes,
  remember,
  sequenceSound,
  sequenceVibration,
  shortestVibration,
  soundCount,
  threeFigures,
  vibrationCount,
  vibrationMath,
}

// Initialize games
extension LoadGame on GameMode {
  BasicGame load(
      {@required int players, @required GamePageState gamePageState}) {
    switch (this) {
      case GameMode.colorCalculation:
        return CalculationGame(players, gamePageState, colorMode: true);
      case GameMode.colorUsed:
        return ColorUsedGame(players, gamePageState);
      case GameMode.farm:
        return FarmGame(players, gamePageState);
      case GameMode.lightCount:
        return LightCountGame(players, gamePageState);
      case GameMode.lightsOn:
        return LightsOnGame(players, gamePageState);
      case GameMode.listenMath:
        return ListenMathGame(players, gamePageState);
      case GameMode.mathCalculation:
        return CalculationGame(players, gamePageState);
      case GameMode.objectSpeed:
        return ObjectSpeedGame(players, gamePageState);
      case GameMode.recognize:
        return RecognizeGame(players, gamePageState);
      case GameMode.reflexes:
        return ReflexesGame(players, gamePageState);
      case GameMode.remember:
        return RememberGame(players, gamePageState);
      case GameMode.sequenceSound:
        return SequenceGame(players, gamePageState);
      case GameMode.sequenceVibration:
        return SequenceGame(players, gamePageState, useVibration: true);
      case GameMode.shortestVibration:
        return ShortestVibrationGame(players, gamePageState);
      case GameMode.soundCount:
        return CountGame(players, gamePageState);
      case GameMode.threeFigures:
        return ThreeFiguresGame(players, gamePageState);
      case GameMode.vibrationCount:
        return CountGame(players, gamePageState, useVibration: true);
      case GameMode.vibrationMath:
        return ListenMathGame(players, gamePageState, useVibration: true);
      default:
        return null;
    }
  }
}
