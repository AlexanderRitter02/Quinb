import 'package:flutter/foundation.dart';

import 'package:quinb/i18n/animal.i18n.dart';

// Animals used in some games
enum Animal { cat, duck, dog }

extension AnimalInfo on Animal {
  // Animal name
  // i18n: 'cat'.i18n 'duck'.i18n 'dog'.i18n
  String get name => describeEnum(this).i18n;

  // Animal sound filename
  String get fileName => '${describeEnum(this)}_1.wav';

  // Animal emoji
  String get emoji {
    if (this == Animal.cat) return '🐈';
    if (this == Animal.dog) return '🐕';
    if (this == Animal.duck) return '🦆';
    return 'Invalid';
  }
}
