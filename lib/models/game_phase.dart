// Game phases:
// 1) prePhase:
// ---> the game does not expect any input by players
// ---> the game could display things or make sounds
// 2) main:
// ---> the game expects input by players
// ---> the game could display things or make sounds
// 4) postMain:
// ---> the game is locked, input is being (or going to be) processed
// 3) pause:
// ---> game is paused, no inputs nor outputs
// 4) end:
// ---> game is over, nothing is going to happen anymore
enum GamePhase { preMain, main, postMain, pause, end }
