import 'dart:io';
import 'dart:ui';

import 'package:quinb/models/difficulty.dart';

// App settings
class Settings {
  // Game difficulty
  Difficulty difficulty;

  // Points required to win
  int requiredPoints;

  // Time available in-game
  int timeAvailable;

  // Maximum number of rounds
  int maxRounds;

  // Default number of players
  int players;

  // True if every game start paused
  bool startPaused;

  // True if the app should force volume up when the app starts
  bool enableVolumeOnStart;

  // True if sound-based game should be shown
  bool enableSoundGames;

  // True if it is the first run of the app
  bool firstRun;

  // App language
  String _languageCode;
  bool get useSystemLanguage => _languageCode == null;
  String get languageCode => _languageCode ?? Platform.localeName;
  Locale get locale => Locale(languageCode.split('_').first);
  set locale(Locale locale) {
    _languageCode = locale?.toString();
  }

  Settings(Map<String, dynamic> json)
      : difficulty = Difficulty.values[json['difficulty'] ?? 2],
        requiredPoints = json['requiredPoints'] ?? 7,
        timeAvailable = json['timeAvailable'] ?? 7,
        maxRounds = json['maxRounds'] ?? 24,
        players = json['players'] ?? 2,
        startPaused = json['startPaused'] ?? true,
        enableVolumeOnStart = json['enableVolumeOnStart'],
        enableSoundGames = json['enableSoundGames'] ?? true,
        firstRun = json['firstRun'] ?? true,
        _languageCode = json['languageCode'];

  Map<String, dynamic> toJson() => {
        'difficulty': difficulty.index,
        'requiredPoints': requiredPoints,
        'timeAvailable': timeAvailable,
        'maxRounds': maxRounds,
        'players': players,
        'startPaused': startPaused,
        'enableVolumeOnStart': enableVolumeOnStart,
        'enableSoundGames': enableSoundGames,
        'firstRun': firstRun,
        'languageCode': _languageCode,
      };
}
