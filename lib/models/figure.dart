import 'package:flutter/material.dart';

enum Shape { square, circle, triangle }

class Figure {
  // Text inside
  String text;

  // Color
  Color color;

  // Shape
  Shape shape;

  Figure(this.text, this.color, {this.shape = Shape.square});

  @override
  bool operator ==(o) =>
      o is Figure && text == o.text && color == o.color && shape == o.shape;

  @override
  int get hashCode => text.hashCode ^ color.hashCode ^ shape.hashCode;
}
