import 'dart:math';

// Get the value of a random color
extension RandomColor on Random {
  int nextColorValue(int max) =>
      0xff000000 + (nextInt(max) << 16) + (nextInt(max) << 8) + nextInt(max);
}
