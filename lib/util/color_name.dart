import 'package:flutter/material.dart';
import 'package:quinb/i18n/color_name.i18n.dart';

// Add name getter to Color
extension ColorName on Color {
  String get name {
    if (value == Colors.blue.value) return 'Blue'.i18n;
    if (value == Colors.blueAccent.value) return 'Blue'.i18n;
    if (value == Colors.red.value) return 'Red'.i18n;
    if (value == Colors.redAccent.value) return 'Red'.i18n;
    if (value == Colors.green.value) return 'Green'.i18n;
    if (value == Colors.orange.value) return 'Yellow'.i18n;
    if (value == Colors.orangeAccent.value) return 'Yellow'.i18n;
    if (value == Colors.purple.value) return 'Purple'.i18n;
    if (value == Colors.purpleAccent.value) return 'Purple'.i18n;
    if (value == Colors.black.value) return 'Black'.i18n;
    if (value == Colors.brown.value) return 'Brown'.i18n;
    return 'Invalid'.i18n;
  }
}
