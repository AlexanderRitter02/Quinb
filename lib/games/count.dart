import 'dart:async';
import 'dart:math';
import 'package:vibration/vibration.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/games/audio_mixin.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';

// Count repetitions
class CountGame extends BasicGame with AudioGame {
  // True if outputs are vibrations instead of sounds
  final bool useVibration;

  CountGame(int players, GamePageState gamePage, {this.useVibration = false})
      : super(players, gamePage);

  // Sound played
  final String beep = 'sound_17.wav';

  @override
  void prePhase() async {
    int lowestValue = Random().nextInt(10) + max(1, settings.difficulty.index);
    optionsAvailable = [for (var i = 0; i < 5; i++) lowestValue + i].toOptions()
      ..shuffle();
    correctValue =
        optionsAvailable[Random().nextInt(optionsAvailable.length)].value;
    for (var i = 0; i < correctValue; i++) {
      if (!isPrePhase) break;
      await Future.delayed(Duration(milliseconds: (i == 0 ? 1000 : 200)));
      await (useVibration
          ? Vibration.vibrate(duration: 150)
          : audioCache.play(beep));
    }
  }
}
