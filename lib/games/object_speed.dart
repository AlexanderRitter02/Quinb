import 'dart:async';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/movement.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/resources/colors.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/color_name.dart';
import 'package:quinb/util/local_data_controller.dart';

// Find the fastest object
class ObjectSpeedGame extends BasicGame {
  ObjectSpeedGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  final int objectCount = 3 + settings.difficulty.index;

  // Offset Animation list
  List<Animation<Offset>> animationList;

  // Animation controlled that could be used by the game
  List<AnimationController> animationControllerList;

  // List of objects movements
  List<Movement> movements;
  List<Movement> get _movementsSorted =>
      List.from(movements)..sort((a, b) => (b.speed - a.speed).round());

  @override
  void init() {
    animationList = List<Animation<Offset>>(objectCount);
    animationControllerList = List.generate(
        objectCount, (index) => AnimationController(vsync: gamePage));
    super.init();
  }

  @override
  void pause() {
    super.pause();
    if (isPaused) {
      animationControllerList.forEach((ac) {
        ac.stop(canceled: false);
      });
    } else {
      animationControllerList.forEach((ac) {
        if (ac.duration != null) ac.repeat(reverse: true);
      });
    }
  }

  @override
  void prePhase() async {
    movements = List.generate(objectCount, (i) => Movement(colorList[i].value));
    optionsAvailable = List.generate(objectCount,
        (index) => Option(colorList[index].value, text: colorList[index].name));
    calculateAnimations();
    optionsAvailable.removeWhere((element) => [
          if (optionsAvailable.length > 5)
            _movementsSorted[_movementsSorted.length - 1].id,
          if (optionsAvailable.length > 6)
            _movementsSorted[_movementsSorted.length - 2].id
        ].contains(element.value));
    correctValue = _movementsSorted.first.id;
    await Future.delayed(const Duration(seconds: 2));
  }

  // Set animationControllerList values
  void calculateAnimations() {
    for (var i = 0; i < animationControllerList.length; i++) {
      animationControllerList[i].duration = movements[i].duration;
      animationControllerList[i].repeat(reverse: true);
      animationList[i] =
          Tween<Offset>(begin: movements[i].begin, end: movements[i].end)
              .animate(animationControllerList[i]);
    }
  }

  @override
  Widget widget() => (isMainPhase || isPostPhase) && phaseLog.isNotEmpty
      ? Container(
          height: 275,
          child: InkWell(
            onTap: isMainPhase ? pause : null,
            child: Container(
              alignment: AlignmentDirectional.topStart,
              child: Stack(
                children: <Widget>[
                  for (int i = 0; i < animationList.length; i++)
                    SlideTransition(
                      position: animationList[i],
                      child: CircleAvatar(backgroundColor: colorList[i]),
                    ),
                ],
              ),
            ),
          ),
        )
      : super.widget();

  @override
  void dispose() {
    animationControllerList.forEach((ac) {
      ac.dispose();
    });
    animationControllerList.clear();
    super.dispose();
  }
}
