import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/resources/colors.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/color_name.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/i18n/color_used.i18n.dart';

// What color is it
class ColorUsedGame extends BasicGame {
  ColorUsedGame(int players, GamePageState gamePage) : super(players, gamePage);

  @override
  String get tip {
    switch (element) {
      case Element.textColor:
        return 'What color is the text?'.i18n;
      case Element.background:
        return 'What color is the background?'.i18n;
      case Element.textString:
        return 'What color is written?'.i18n;
      default:
        return '';
    }
  }

  @override
  bool get useTimer => false;

  // Time between each update
  final int _time = 2000 + (5 - settings.difficulty.index) * 200;

  // Asking for background, text or text color
  Element element;

  // Colors used
  Color backgroundColor;
  Color textString;
  Color textColor;

  List<Color> colors = [];

  @override
  void init() {
    colors = colorList.sublist(0, 4);
    optionsAvailable.clear();
    for (var i = 0; i < colors.length; i++) {
      optionsAvailable.add(Option(colors[i].value, text: colors[i].name));
    }
    _updateColors();
    super.init();
  }

  @override
  void prePhase() async {
    _updateColors();
    await Future.delayed(const Duration(seconds: 2));
  }

  @override
  void mainPhase() {
    updater = Timer.periodic(Duration(milliseconds: _time), (_) {
      if (!isMainPhase || isPostPhase) return;
      _updateColors();
      gamePage.refresh();
    });
  }

  // Update background, text and text color
  void _updateColors() {
    colors = colorList.sublist(0, 4);
    optionsAvailable.shuffle();
    backgroundColor = colors[Random().nextInt(colors.length)];
    colors.remove(backgroundColor);

    textString = colors[Random().nextInt(colors.length)];
    colors.remove(textString);

    textColor = colors[Random().nextInt(colors.length)];
    colors.remove(textColor);

    element = Element.values[Random().nextInt(Element.values.length)];
    switch (element) {
      case Element.background:
        correctValue = backgroundColor.value;
        break;
      case Element.textColor:
        correctValue = textColor.value;
        break;
      case Element.textString:
        correctValue = textString.value;
        break;
    }
  }

  @override
  Widget widget() => isMainPhase || isPostPhase
      ? Wrap(
          children: [
            InkWell(
              onTap: isMainPhase ? pause : null,
              child: Container(
                height: 240,
                width: MediaQuery.of(gamePage.context).size.width,
                color: backgroundColor,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    if (playerList.length > 1)
                      RotatedBox(quarterTurns: 2, child: _widget()),
                    _widget(),
                  ],
                ),
              ),
            ),
          ],
        )
      : super.widget();

  Widget _widget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          textString.name.toUpperCase(),
          style: TextStyle(
            color: textColor,
            fontSize: 40,
            fontWeight: FontWeight.w700,
            letterSpacing: 6,
            shadows: [
              const Shadow(offset: Offset(-2, -2), color: Colors.white),
              const Shadow(offset: Offset(2, -2), color: Colors.white),
              const Shadow(offset: Offset(2, 2), color: Colors.white),
              const Shadow(offset: Offset(-2, 2), color: Colors.white)
            ],
          ),
        ),
        Text(tip, style: const TextStyle(color: Colors.white, fontSize: 20))
      ],
    );
  }
}

enum Element { background, textString, textColor }
