import 'dart:async';
import 'package:flutter/material.dart';

import 'package:quinb/models/game_detail.dart';
import 'package:quinb/models/game_phase.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/models/player.dart';
import 'package:quinb/resources/colors.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/i18n/abstract_basic.i18n.dart';

// Generic game
abstract class BasicGame {
  // Active GamePageState
  final GamePageState gamePage;

  BasicGame(int players, this.gamePage) {
    for (var index = 0; index < players; index++) {
      playerList.add(Player(color: playerColor[index], id: index));
    }
    init();
  }

  // Game info
  GameDetails get info =>
      gameList.firstWhere((detail) => detail.mode == gamePage.currentMode);

  // Game phase
  GamePhase _phase;
  bool get isPaused => _phase == GamePhase.pause;
  bool get isStarting => _phase == null;
  bool get isPrePhase => _phase == GamePhase.preMain;
  bool get isMainPhase => _phase == GamePhase.main;
  bool get isPostPhase => _phase == GamePhase.postMain;
  bool get isOver => _phase == GamePhase.end;

  // History of phase changes
  List<GamePhase> phaseLog = [];

  // round count
  int roundCount = 0;

  // List of players in game
  List<Player> playerList = [];

  // List of players sorted by points
  List<Player> get ranking => List.from(playerList).cast<Player>()
    ..sort((a, b) => b.points.compareTo(a.points));

  // True if timer is enabled
  final bool useTimer = true;

  // Enable haptic feedback on tap on options
  final bool enableFeedback = true;

  // Time passed
  int elapsedTime = 0;
  bool get timeOver => elapsedTime == settings.timeAvailable;

  // List of available options
  List<Option> optionsAvailable = Option.single;

  // Value of the correct answer
  int correctValue;

  // Timer that game could use to update itself
  // This is not the 7 seconds timer
  Timer updater;

  // Initialize variables. It is called once.
  @mustCallSuper
  void init() async {
    if (settings.startPaused) {
      pause();
    } else {
      await Future.delayed(const Duration(seconds: 3));
      if (isOver) return;
      newPhase(GamePhase.preMain);
      if (canStart()) newTurn();
    }
  }

  // True if a new round can start
  bool canStart() => true;

  // Start a new match. It is called after init and after every setInput()
  @mustCallSuper
  void newTurn() async {
    ++roundCount;
    await prePhase();
    if (isOver) return;
    newPhase(GamePhase.main);
    elapsedTime = 0;
    await mainPhase();
  }

  // Game phases, the logic of the game must be placed here
  void prePhase() {}
  void mainPhase() {}

  // Check if the answer is correct
  bool validateInput(int input) => input == correctValue;

  // Input inserted by a player
  void setInput({bool correct, int playerIndex = 0}) {
    newPhase(GamePhase.preMain);
    updater?.cancel();
    calculatePoints(playerIndex: playerIndex, correct: correct);
  }

  // Calculate player points after input
  // If nobody won it starts a new match
  void calculatePoints({@required int playerIndex, @required bool correct}) {
    if (correct != null) playerList[playerIndex].points += (correct ? 1 : -1);
    if (playerList[playerIndex].points == settings.requiredPoints ||
        (playerList[playerIndex].points == 0 - settings.requiredPoints &&
            playerList.length <= 2) ||
        roundCount == settings.maxRounds) {
      endGame();
    } else if (canStart() && !randomMode) {
      newTurn();
    }
  }

  // Method called if someone won/lost
  void endGame() {
    newPhase(GamePhase.end);
    dispose();
  }

  // Change phase
  void newPhase(GamePhase newPhase, {bool forced = false}) {
    if (newPhase == GamePhase.pause && (isPaused || isPostPhase)) return;
    if (isPaused && !forced) {
      phaseLog.addAll([newPhase, GamePhase.pause]);
    } else {
      _phase = newPhase;
      phaseLog.add(_phase);
      gamePage.refresh();
    }
    // print(_phase);
  }

  // Pause or un-pause the game
  @mustCallSuper
  void pause() {
    if (!isPaused) {
      newPhase(GamePhase.pause);
    } else {
      if (phaseLog.length == 1) {
        newPhase(GamePhase.preMain, forced: true);
        if (canStart()) newTurn();
      } else {
        newPhase(phaseLog[phaseLog.length - 2], forced: true);
        gamePage.showExitOptions = false;
      }
    }
  }

  // Text shown if isPrePhase
  String get preTip => info.subtitle;

  // Text shown if isMain
  String get tip => info.inGameTip;

  // Game widget
  Widget widget() {
    var _text = _mainText();
    return Expanded(
      child: InkWell(
        onTap: () {
          if (isPrePhase) {
            tapOnPrePhase();
          } else if (isMainPhase || isPaused) {
            pause();
          }
        },
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              if (playerList.length > 1)
                RotatedBox(quarterTurns: 2, child: _textWidget(_text)),
              _textWidget(_text)
            ],
          ),
        ),
      ),
    );
  }

  // Text shown in the main widget
  String _mainText() {
    var _text = '';
    if (isOver) {
      if (roundCount == settings.maxRounds &&
          ranking.first.points != settings.requiredPoints) {
        _text = 'Too many rounds'.i18n + ' ($roundCount). ';
        if (playerList.length == 1) _text += '\nYou lost!'.i18n;
      } else if (ranking.first.points != settings.requiredPoints) {
        return 'Too many mistakes. '.i18n +
            (playerList.length == 1
                ? '\nYou lost!'.i18n
                : '%s won!'.i18n.fill([ranking.first.name]));
      }
      if (playerList.length >= 2) {
        if (ranking.first.points == 0) {
          _text += 'Nobody won!'.i18n;
        } else {
          _text += '%s won!'.i18n.fill([ranking.first.name]);
        }
      } else if (ranking.first.points == settings.requiredPoints) {
        _text += 'You won!'.i18n;
      }
    } else if (phaseLog.length <= 1) {
      _text = info.title;
    } else if (isPaused) {
      _text = 'P A U S E'.i18n;
    } else {
      _text = (isMainPhase || isPostPhase) ? tip : preTip;
    }
    return _text;
  }

  // Widget that contains the text
  Widget _textWidget(String text) => Center(
        child: FittedBox(
          fit: BoxFit.fitWidth,
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey[800], fontSize: 24),
          ),
        ),
      );

  // Function executed when tapping on the main widget if isPrePhase
  void tapOnPrePhase() {}

  // Dispose the game
  @mustCallSuper
  void dispose() {
    updater?.cancel();
    _phase = GamePhase.end;
  }
}
