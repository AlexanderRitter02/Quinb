import 'dart:async';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/games/audio_mixin.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/resources/sound_assets.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/i18n/recognize.i18n.dart';

// Recognize a sound or sequence of sounds
class RecognizeGame extends BasicGame with AudioGame {
  RecognizeGame(int players, GamePageState gamePage) : super(players, gamePage);

  @override
  String get preTip => samplesToBePlayed > 0
      ? 'Tap here to listen to the sample'.i18n
      : 'Tap here to start'.i18n;

  @override
  String get tip => (sequenceLength > 1
      ? 'Tap when you hear the previous sequence'.i18n
      : 'Tap when you hear the previous sound'.i18n);

  @override
  final bool useTimer = false;

  @override
  final bool enableFeedback = false;

  // Length of the sequence to be recognized
  final int sequenceLength = max(1, settings.difficulty.index - 1);

  // Times the player can play the sample
  static final int maxSamples = 2;

  // Number of sample yet to be played before playing
  int samplesToBePlayed = maxSamples;

  @override
  bool canStart() => samplesToBePlayed == 0;

  // Sounds to be recognized by players
  List<String> sequenceToBeRecognized = [];

  // Playing sequence
  List<String> sequence;

  // Index in sequence of the sound being played
  int currentlyPlayingIndex;

  // True if it is playing a sample sound
  bool _playingSample = false;

  @override
  void tapOnPrePhase() async {
    if (samplesToBePlayed == maxSamples) generateSequences();
    if (_playingSample) return;
    if (--samplesToBePlayed >= 0) {
      _playingSample = true;
      for (var index = 0; index < sequenceLength; index++) {
        await audioCache.play(sequenceToBeRecognized[index]);
        await Future.delayed(const Duration(milliseconds: (800)));
      }
      gamePage.refresh();
      _playingSample = false;
    } else {
      newTurn();
    }
  }

  @override
  void prePhase() {
    samplesToBePlayed = maxSamples;
    currentlyPlayingIndex = -1;
  }

  @override
  void mainPhase() {
    updater = Timer.periodic(const Duration(seconds: 1), (_) {
      if (isPaused || isPostPhase) return;
      _playNext();
      correctValue = (currentlyPlayingIndex >= sequenceLength &&
              listEquals(
                  sequence.sublist(currentlyPlayingIndex - sequenceLength + 1,
                      currentlyPlayingIndex + 1),
                  sequenceToBeRecognized))
          ? Option.tap.value
          : 0;
    });
  }

  // Generate sequence of sounds
  void generateSequences() {
    sequence =
        List.generate(20, (_) => soundList[Random().nextInt(soundList.length)]);
    var _startSublist = Random().nextInt(sequence.length - sequenceLength);
    sequenceToBeRecognized =
        sequence.sublist(_startSublist, _startSublist + sequenceLength);
  }

  // Play next sound in sequence
  void _playNext() {
    if (++currentlyPlayingIndex >= sequence.length) currentlyPlayingIndex = 0;
    audioCache.play(sequence[currentlyPlayingIndex]);
  }

  @override
  Widget widget() => isPrePhase
      ? Expanded(
          child: InkWell(
            enableFeedback: false,
            onTap: tapOnPrePhase,
            child: Stack(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                        flex: samplesToBePlayed,
                        child: Container(color: Colors.red[200])),
                    Expanded(
                        flex: maxSamples - samplesToBePlayed,
                        child: Container(color: Colors.white)),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      if (playerList.length > 1)
                        RotatedBox(
                          quarterTurns: 2,
                          child: Center(
                            child: FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Text(
                                preTip,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.grey[800], fontSize: 24),
                              ),
                            ),
                          ),
                        ),
                      Container(
                        child: Center(
                          child: FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Text(
                              preTip,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.grey[800], fontSize: 24),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      : super.widget();
}
