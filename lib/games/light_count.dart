import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';

// Count the lights
class LightCountGame extends BasicGame {
  LightCountGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  // Total number of lights
  final int lights = 16 + settings.difficulty.index * 4;

  // Time the lights stay on
  final int _time = 300 + (6 - settings.difficulty.index) * 100;

  // Lights off
  List<int> lightsOff = [];

  // Lights on now
  List<int> currentLights = [];

  @override
  void prePhase() async {
    lightsOff = List<int>.generate(lights, (i) => i);
    for (var i = 0;
        i < 4 + Random().nextInt(settings.difficulty.index + 1);
        i++) {
      if (!isPrePhase) return;
      currentLights.clear();
      for (var i = 0;
          i < 1 + Random().nextInt(settings.difficulty.index + 3);
          i++) {
        if (lightsOff.isEmpty) return;
        var light = lightsOff[Random().nextInt(lightsOff.length)];
        currentLights.add(light);
        lightsOff.remove(light);
      }
      await Future.delayed(Duration(milliseconds: _time));
      gamePage.refresh();
    }
    correctValue = lights - lightsOff.length;
    var startIndex = correctValue - 4 + Random().nextInt(4);
    optionsAvailable =
        List<int>.generate(4, (i) => startIndex + i + 1).toOptions();
    await Future.delayed(Duration(milliseconds: _time));
  }

  @override
  Widget widget() => isPrePhase
      ? Wrap(
          children: [
            GridView.count(
              physics: const ClampingScrollPhysics(),
              shrinkWrap: true,
              crossAxisCount: 4 + settings.difficulty.index,
              children: List.generate(lights, (index) {
                return Container(
                  decoration: BoxDecoration(
                    color: currentLights.contains(index)
                        ? Colors.orange
                        : Colors.grey[800],
                    border: Border.all(color: Colors.black),
                  ),
                );
              }),
            ),
          ],
        )
      : super.widget();
}
