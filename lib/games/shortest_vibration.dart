import 'dart:async';
import 'package:vibration/vibration.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/i18n/shortest_vibration.i18n.dart';

// Find the shortest vibration
class ShortestVibrationGame extends BasicGame {
  ShortestVibrationGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  List<Option> optionsAvailable = [
    Option(0, text: 'First'.i18n),
    Option(1, text: 'Second'.i18n),
    Option(2, text: 'Third'.i18n),
    Option(3, text: 'Fourth'.i18n),
  ];

  // Durations of vibrations
  List<int> durations = List.generate(
      4, (index) => 125 + index * (15 + (4 - settings.difficulty.index) * 5));

  @override
  void prePhase() async {
    durations.shuffle();
    for (var i = 0; i < optionsAvailable.length; i++) {
      if (!isPrePhase) return;
      await Future.delayed(Duration(milliseconds: (i == 0 ? 1000 : 500)));
      await Vibration.vibrate(duration: durations[i]);
    }
    correctValue = durations.indexOf((List.from(durations)..sort()).first);
  }
}
