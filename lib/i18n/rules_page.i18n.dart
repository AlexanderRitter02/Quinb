// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Close''': '''Chiudi''',
          '''Back''': '''Indietro''',
          '''OK''': '''OK''',
          '''Next''': '''Avanti''',
        },
        'es': {
          '''Close''': '''Cerrar''',
          '''Back''': '''Atrás''',
          '''OK''': '''Aceptar''',
          '''Next''': '''Siguiente''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
