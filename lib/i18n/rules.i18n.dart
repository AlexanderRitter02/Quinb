// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Games''': '''Giochi''',
          '''Points''': '''Punti''',
        },
        'es': {
          '''Quinb is a multiplayer reaction game.\nTrain your mind and your reflexes while having fun!\n\nSwipe to right to learn how to play.''':
              '''Quinb es un juego multijugador de reacciones.\n¡Entrena tu mente y tus reflejos mientras te diviertes!\n\nDesliza a la derecha para aprender cómo jugar.''',
          '''Games''': '''Juegos''',
          '''Quinb contains many minigames based on 3 different categories:\nlogic, audio, vibration.''':
              '''Quinb contiene muchos minijuegos basados en 3 categorías distintas:\nlógica, sonido, vibración.''',
          '''Points''': '''Puntos''',
          '''In every game you have to answer questions as fast as you can.\nIf the answer is correct you score a point, otherwise you lose one.\n''':
              '''En cada juego tienes que responder preguntas tan rápido como puedas.\nSi la respuesta es correcta sumas un punto, si no, pierdes uno.\n''',
          '''Goal''': '''Objetivo''',
          '''Each match consists of a succession of different minigames.\nThe goal is to score 7 points before your opponents.''':
              '''Cada partida consiste en una sucesión de distintos minijuegos.\nEl objetivo es sumar 7 puntos antes que tus oponentes.''',
          '''Multiplayer''': '''Multijugador''',
          '''Quinb can be played by 4 players on the same device, but you can also play alone if you want.''':
              '''Quinb admite hasta 4 jugadores en el mismo dispositivo, pero también puedes jugar de manera individual, si quieres.''',
          '''That's all''': '''Eso es todo''',
          '''You will learn the rules of each minigame by playing.\n\nKeep in mind that this game is way more fun if played with friends!''':
              '''Aprenderás las reglas de cada minijuego al jugarlo.\n\n¡Ten presente que este juego es mucho más divertido si lo juegas con amigos/as!''',
        },
        'ru': {
          '''Games''': '''Игры''',
          '''Points''': '''Очки''',
          '''Goal''': '''Цель''',
          '''Multiplayer''': '''Сетевая игра''',
          '''That's all''': '''Это всё''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
