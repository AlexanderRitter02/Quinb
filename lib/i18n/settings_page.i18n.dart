// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''SETTINGS''': '''IMPOSTAZIONI''',
          '''Restore''': '''Ripristina''',
          '''Language''': '''Lingua''',
          '''App language''': '''Lingua applicazione''',
          '''System default''': '''Predefinita di sistema''',
        },
        'es': {
          '''SETTINGS''': '''AJUSTES''',
          '''Restore''': '''Restaurar''',
          '''Pause between rounds''': '''Pausar entre rondas''',
          '''Wait before starting the next round''':
              '''Esperar antes de empezar la siguiente ronda''',
          '''Difficulty''': '''Dificultad''',
          '''Difficulty of the games''': '''Dificultad de los juegos''',
          '''Points required''': '''Puntos necesarios''',
          '''Points required to win''': '''Puntos necesarios para ganar''',
          '''Round duration''': '''Duración de la ronda''',
          '''Time available each round (in seconds)''':
              '''Tiempo disponible cada ronda (en segundos)''',
          '''Rounds''': '''Rondas''',
          '''Maximum number of rounds''': '''Máximo número de rondas''',
        },
        'ru': {
          '''SETTINGS''': '''НАСТРОЙКИ''',
          '''Restore''': '''Восстановить''',
          '''Pause between rounds''': '''Пауза между раундами''',
          '''Wait before starting the next round''':
              '''Подождите пока начнётся следующий раунд''',
          '''Difficulty''': '''Сложность''',
          '''Difficulty of the games''': '''Сложность игр''',
          '''Points required''': '''Очков требуется''',
          '''Points required to win''': '''Очков требуется для победы''',
          '''Round duration''': '''Продолжительность раунда''',
          '''Time available each round (in seconds)''':
              '''Время доступное на раунд (в секундах)''',
          '''Rounds''': '''Раунды''',
          '''Maximum number of rounds''': '''Максимальное число раундов''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
