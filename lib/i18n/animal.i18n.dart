// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''cat''': '''gatto''',
          '''duck''': '''anatra''',
          '''dog''': '''cane''',
        },
        'es': {
          '''cat''': '''gato''',
          '''duck''': '''pato''',
          '''dog''': '''perro''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
