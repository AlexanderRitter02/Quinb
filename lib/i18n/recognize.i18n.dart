// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Tap here to listen to the sample''':
              '''Tocca quando senti il suono di prima''',
        },
        'es': {
          '''Tap here to listen to the sample''':
              '''Toca aquí para escuchar la muestra''',
          '''Tap here to start''': '''Toca aquí para empezar''',
          '''Tap when you hear the previous sequence''':
              '''Toca cuando oigas la secuencia anterior''',
          '''Tap when you hear the previous sound''':
              '''Toca cuando oigas el sonido anterior''',
        },
        'ru': {
          '''Tap here to listen to the sample''':
              '''Нажми сюда для прослушивания примера''',
          '''Tap here to start''': '''Нажми здесь для старта''',
          '''Tap when you hear the previous sequence''':
              '''Нажми когда ты услышишь предыдушее звучание''',
          '''Tap when you hear the previous sound''':
              '''Нажми когда ты услышишь предыдуший звук''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
