// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Blue''': '''Blu''',
          '''Red''': '''Rosso''',
        },
        'es': {
          '''Blue''': '''Azul''',
          '''Red''': '''Rojo''',
          '''Green''': '''Verde''',
          '''Yellow''': '''Amarillo''',
          '''Purple''': '''Morado''',
          '''Black''': '''Negro''',
          '''Brown''': '''Marrón''',
          '''Invalid''': '''Inválido''',
        },
        'ru': {
          '''Blue''': '''Синий''',
          '''Red''': '''Красный''',
          '''Green''': '''Зелёный''',
          '''Yellow''': '''Жёлтый''',
          '''Purple''': '''Фиолетовый''',
          '''Black''': '''Чёрный''',
          '''Brown''': '''Коричневый''',
          '''Invalid''': '''Неправильный''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
