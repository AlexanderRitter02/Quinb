// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Remember''': '''Ricorda''',
        },
        'es': {
          '''Remember''': '''Recordar''',
          '''Remember the colors and letters that appear''':
              '''Recuerda los colores y las letras que aparecen''',
          '''Remember the sequence of colors and letters that will appear on the screen''':
              '''Recuerda la secuencia de colores y letras que aparecerá en la pantalla''',
          '''Three figures''': '''Tres figuras''',
          '''Find three identical figures''':
              '''Encuentra tres figuras idénticas''',
          '''Tap when there are 3 identical figures''':
              '''Toca cuando hay 3 figuras idénticas''',
          '''Tap the screen when you see at least 3 figures that have the same color and the same letter inside''':
              '''Toca la pantalla cuando veas al menos 3 figuras que tienen el mismo color y la misma letra dentro''',
          '''Vibration repetitions''': '''Repeticiones de vibraciones''',
          '''Count how many times you hear the vibration''':
              '''Cuenta cuántas veces sientes la vibración''',
          '''How many times have you heard the vibration?''':
              '''¿Cuántas veces has sentido la vibración?''',
          '''Count the lights''': '''Cuenta las luces''',
          '''Count how many lights come on''':
              '''Cuenta cuántas luces se encienden''',
          '''How many light have come on?''':
              '''¿Cuántas luces han aparecido?''',
          '''Some lights are about to come on in rapid succession.\n\nCounts how many lights come on.''':
              '''Algunas luces aparecerán en una sucesión rápida.\n\nCuenta cuántas luces aparecen.''',
          '''Sound repetitions''': '''Repeticiones de sonido''',
          '''Count how many times you hear the sound''':
              '''Cuenta cuántas veces oyes el sonido''',
          '''How many times have you heard the sound?''':
              '''¿Cuántas veces has escuchado el sonido?''',
          '''Shortest vibration''': '''Vibración más corta''',
          '''Find the shortest vibration''':
              '''Encuentra la vibración más corta''',
          '''Which one was the shortest vibration?''':
              '''¿Cuál era la vibración más corta?''',
          '''Try to figure out which one is the shortest vibration among four''':
              '''Intenta averiguar cuál es la vibración más corta entre cuatro''',
          '''Math calculation''': '''Cálculo matemático''',
          '''Solve the equations''': '''Resuelve las ecuaciones''',
          '''Solve the equations in your head as fast as you can''':
              '''Resuelve las ecuaciones en tu cabeza tan rápido como puedas''',
          '''Lights on''': '''Luces encendidas''',
          '''Count if there are more lights on than off''':
              '''Cuenta si hay más luces encendidas que apagadas''',
          '''Are there more lights on than off?''':
              '''¿Hay más luces encendidas que apagadas?''',
          '''Tap the screen if there are more yellow squares than black ones''':
              '''Toca la pantalla si hay más cuadrados amarillos que negros''',
          '''Vibration sequence''': '''Secuencia de vibraciones''',
          '''Remember the sequence you hear''':
              '''Recuerda la secuencia que escuchas''',
          '''Which sequence was it?''': '''¿Qué secuencia era?''',
          '''Listen to the vibration sequence and remember it.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration''':
              '''Escucha la secuencia de vibraciones y recuérdala.\n\n__ es la representación de una vibración larga\n-  es la representación de una vibración corta''',
          '''Mix Colors''': '''Mezclar colores''',
          '''Mix the colors''': '''Mezcla los colores''',
          '''Try to figure out which color results from the color mixing''':
              '''Intenta averiguar qué color resulta de la mezcla''',
          '''Audio sequence''': '''Secuencia de sonidos''',
          '''Listen to the sounds sequence and remember it''':
              '''Escucha la secuencia de sonidos y recuérdala''',
          '''Reflexes''': '''Reflejos''',
          '''Tap at the right moment''': '''Toca en el momento exacto''',
          '''As soon as the screen becomes colored, tap the option of the same color.\n\nKeep in mind that you have little time before the screen turns white again.''':
              '''Tan pronto como se coloree la pantalla, toca la opción del mismo color.\n\nTen presente que tienes poco tiempo antes de que la pantalla se vuelva blanca de nuevo.''',
          '''Fastest object''': '''Objeto más rápido''',
          '''Find the fastest object''': '''Encuentra el objeto más rápido''',
          '''Try to figure out which one is the fastest moving object''':
              '''Intenta averiguar cuál es el objeto que se mueve más rápido''',
          '''What color is it''': '''Qué color es''',
          '''Recognize which color is used''': '''Reconoce qué color se usa''',
          '''Answer as fast as you can which color is written or which color is used for the background or for the text''':
              '''Responde tan rápido como puedas qué color está escrito o qué color se usa para el fondo o para el texto''',
          '''Listen and calculate''': '''Escucha y calcula''',
          '''Add or subtract based on what you hear''':
              '''Sumar o restar basándose en lo que escuchas''',
          '''What is the result?''': '''¿Cuál es el resultado?''',
          '''Sum if you hear a duck, subtract if you hear a cat.\n\nThe amount of the sum and the subtraction will be shown during the game.''':
              '''Suma si oyes un pato, resta si oyes un gato.\n\nEl total de la suma y resta se mostrará durante el juego.''',
          '''Recognize the sound''': '''Reconoce el sonido''',
          '''Tap the screen when you hear the sound''':
              '''Toca la pantalla cuando escuches el sonido''',
          '''Listen to the sample sound twice here.\n\nThen tap the screen when you hear it during a sequence of sounds.''':
              '''Escucha la muestra de sonido dos veces aquí.\n\nEntonces toca la pantalla cuando la oigas durante la secuencia de sonidos.''',
          '''Hear and calculate''': '''Oír y calcular''',
          '''Add or subtract based on vibrations''':
              '''Sumar o restar basándose en vibraciones''',
          '''Sum if you hear a long vibration, subtract if you hear a short one.\n\nThe amount of the sum and the subtraction will be shown during the game.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration''':
              '''Suma si oyes una vibración larga, resta si oyes una vibración corta.\n\nEl total de la suma y resta se mostrará durante el juego.\n\n__ es la representación de una vibración larga\n-  es la representación de una vibración corta''',
          '''Animal farm''': '''Granja de animales''',
          '''Tap when you hear the correct animal''':
              '''Toca cuando oigas el animal correcto''',
          '''Then tap the screen when you hear the correct animal during a sequence of sounds''':
              '''Entonces toca la pantalla cuando oigas el animal correcto duante la secuencia de sonidos''',
        },
        'ru': {
          '''Remember''': '''Запомни''',
          '''Remember the colors and letters that appear''':
              '''Запомни цвета и надписи''',
          '''Three figures''': '''Три фигуры''',
          '''Find three identical figures''': '''Найди три идентичные фигуры''',
          '''Tap when there are 3 identical figures''':
              '''Нажми когда будет 3 идентичные фигуры''',
          '''Tap the screen when you see at least 3 figures that have the same color and the same letter inside''':
              '''Нажми на экран когда ты увидишь ''',
          '''Count how many times you hear the vibration''':
              '''Посчитай сколько раз ты услышишь вибрацию''',
          '''How many times have you heard the vibration?''':
              '''Сколько раз ты услышал вибрацию?''',
          '''Count the lights''': '''Посчитай цвета''',
          '''Count how many lights come on''':
              '''Посчитай сколько огней будет включенно''',
          '''How many light have come on?''':
              '''Сколько огней было включенно?''',
          '''Count how many times you hear the sound''':
              '''Посчитай сколько раз ты услышишь звук''',
          '''How many times have you heard the sound?''':
              '''Сколько раз ты услышал звук?''',
          '''Shortest vibration''': '''Короткая вибрация''',
          '''Find the shortest vibration''': '''Найди короткую вибрацию''',
          '''Which one was the shortest vibration?''':
              '''Какая вибрация была короткой?''',
          '''Mix Colors''': '''Смесь Цветов''',
          '''Mix the colors''': '''Смешайте цвета''',
          '''Reflexes''': '''Рефлексы''',
          '''Tap at the right moment''': '''Нажмите в нужный момент''',
          '''As soon as the screen becomes colored, tap the option of the same color.\n\nKeep in mind that you have little time before the screen turns white again.''':
              '''Как только экран будет покрашен, нажмите на выбранный цвет.\n\nПомни, что у тебя мало времени до того момента как экран снова станет белым.''',
          '''Fastest object''': '''Быстрый объект''',
          '''Find the fastest object''': '''Найди быстрый объект''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
