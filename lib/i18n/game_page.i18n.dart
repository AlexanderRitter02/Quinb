// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''points''': '''punti''',
          '''Resume''': '''Riprendi''',
        },
        'es': {
          '''points''': '''puntos''',
          '''Resume''': '''Continuar''',
          '''Restart''': '''Volver a empezar''',
          '''Exit''': '''Salir''',
        },
        'ru': {
          '''points''': '''очки''',
          '''Resume''': '''Продолжить''',
          '''Restart''': '''Перезапуск''',
          '''Exit''': '''Выход''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
