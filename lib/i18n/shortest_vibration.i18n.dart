// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''First''': '''Primo''',
          '''Second''': '''Secondo''',
          '''Third''': '''Terzo''',
          '''Fourth''': '''Quarto''',
        },
        'es': {
          '''First''': '''Primero''',
          '''Second''': '''Segundo''',
          '''Third''': '''Tercero''',
          '''Fourth''': '''Cuarto''',
        },
        'ru': {
          '''First''': '''Первый''',
          '''Second''': '''Второй''',
          '''Third''': '''Третий''',
          '''Fourth''': '''Четвёртый''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
