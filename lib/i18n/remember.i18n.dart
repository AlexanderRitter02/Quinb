// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''What color was the %s figure?''':
              '''Di che colore era la %s figura?''',
          '''What letter was in the %s figure?''':
              '''Che lettera c'era nella %s figura?''',
        },
        'es': {
          '''What color was the %s figure?''':
              '''¿De qué color era la %s figura?''',
          '''What letter was in the %s figure?''':
              '''¿Qué letra estaba en la %s figura?''',
          '''1st''': '''1º''',
          '''2nd''': '''2º''',
          '''3rd''': '''3º''',
          '''4th''': '''4º''',
          '''5th''': '''5º''',
          '''6th''': '''6º''',
        },
        'ru': {
          '''What color was the %s figure?''': '''Какого цвета %s фигуре?''',
          '''What letter was in the %s figure?''':
              '''Что написано в %s фигуре?''',
          '''1st''': '''1ый''',
          '''2nd''': '''2ой''',
          '''3rd''': '''3ий''',
          '''4th''': '''4ый''',
          '''5th''': '''5ый''',
          '''6th''': '''6ой''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
