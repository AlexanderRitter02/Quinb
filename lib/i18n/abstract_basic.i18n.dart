// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Too many rounds''': '''Troppi round''',
          '''\nYou lost!''': '''\nHai perso!''',
          '''%s won!''': '''%s ha vinto!''',
        },
        'es': {
          '''Too many rounds''': '''Demasiadas rondas''',
          '''\nYou lost!''': '''\n¡Perdiste!''',
          '''Too many mistakes. ''': '''Demasiados fallos.''',
          '''%s won!''': '''%s ganó!''',
          '''Nobody won!''': '''¡Nadie ganó''',
          '''You won!''': '''¡Has ganado!''',
          '''P A U S E''': '''P A U S A''',
        },
        'ru': {
          '''\nYou lost!''': '''\nТы проиграл!''',
          '''Too many mistakes. ''': '''Слишком много попыток. ''',
          '''%s won!''': '''%s победил!''',
          '''Nobody won!''': '''Никто не победил!''',
          '''You won!''': '''Ты победил!''',
          '''P A U S E''': '''П А У З А''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
