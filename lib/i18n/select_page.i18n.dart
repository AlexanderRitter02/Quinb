// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''GAMES''': '''GIOCHI''',
          '''Rules''': '''Regole''',
          '''Settings''': '''Impostazioni''',
        },
        'es': {
          '''GAMES''': '''JUEGOS''',
          '''Rules''': '''Reglas''',
          '''Sound games''': '''Juegos de sonidos''',
          '''Sound-based games enabled''': '''Juegos de sonidos habilitados''',
          '''Sound-based games disabled''':
              '''Juegos de sonidos deshabilitados''',
          '''players''': '''jugadores/as''',
          '''player''': '''jugador/a''',
          '''Settings''': '''Ajustes''',
          '''Info''': '''Info''',
          '''All''': '''Todo''',
        },
        'ru': {
          '''GAMES''': '''ИГРЫ''',
          '''Rules''': '''Правила''',
          '''Sound games''': '''Звуки игр''',
          '''Sound-based games enabled''': '''Звуковые игры включены''',
          '''Sound-based games disabled''': '''Звуковые игры выключены''',
          '''players''': '''игроки''',
          '''player''': '''игрок''',
          '''Settings''': '''Настройки''',
          '''Info''': '''Информация''',
          '''All''': '''Всё''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
