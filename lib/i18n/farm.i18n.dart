// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Tap when you hear a %s''': '''Tocca quando senti un verso di %s''',
        },
        'es': {
          '''Tap when you hear a %s''': '''Toca cuando oigas un %s''',
        },
        'ru': {
          '''Tap when you hear a %s''': '''Нажми когда ты услышишь %s''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
