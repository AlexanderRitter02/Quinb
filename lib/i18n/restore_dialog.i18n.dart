// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Restore default settings?''':
              '''Ripristinare impostazioni predefinite?''',
          '''Restore''': '''Ripristina''',
        },
        'es': {
          '''Restore default settings?''':
              '''¿Restaurar ajustes predeterminados?''',
          '''Are you sure you want to delete your settings and restore default ones?''':
              '''¿Seguro que quieres borrar tus ajustes y restaurar los predeterminados?''',
          '''Restore''': '''Restaurar''',
          '''Cancel''': '''Cancelar''',
        },
        'ru': {
          '''Restore''': '''Восстановить''',
          '''Cancel''': '''Отмена''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
