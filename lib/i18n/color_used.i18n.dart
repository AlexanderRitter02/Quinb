// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''What color is the text?''': '''Di che colore è il testo?''',
        },
        'es': {
          '''What color is the text?''': '''¿De qué color es el texto?''',
          '''What color is the background?''': '''¿De qué color es el fondo?''',
          '''What color is written?''': '''¿Qué color está escrito?''',
        },
        'ru': {
          '''What color is the text?''': '''Какой цвет текста?''',
          '''What color is the background?''': '''Какой цвет фона?''',
          '''What color is written?''': '''Какой цвет написан?''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
