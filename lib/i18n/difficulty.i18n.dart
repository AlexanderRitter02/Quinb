// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Easy''': '''Facile''',
          '''Hard''': '''Difficile''',
        },
        'es': {
          '''Easiest''': '''El más fácil''',
          '''Easy''': '''Fácil''',
          '''Medium''': '''Medio''',
          '''Hard''': '''Difícil''',
          '''Hardest''': '''El más difícil''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
