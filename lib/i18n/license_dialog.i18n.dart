// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Third Party Licenses''': '''Licenze di terze parti''',
        },
        'es': {
          '''Third Party Licenses''': '''Licencias de terceras partes''',
        },
        'ru': {
          '''Third Party Licenses''': '''Сторонние лицензии''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
