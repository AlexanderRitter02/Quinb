// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''By %s''': '''Da %s''',
          '''App developed by %s''': '''App sviluppata da %s''',
          '''Third Party Licenses''': '''Licenze di terze parti''',
        },
        'es': {
          '''By %s''': '''Por %s''',
          '''App developed by %s''': '''Aplicación desarrollada por %s''',
          '''Version:''': '''Versión:''',
          '''App version''': '''Versión de la aplicación''',
          '''Report bugs''': '''Informar de fallos''',
          '''Report bugs or request new feature''':
              '''Informar de fallos o solicitar nuevas características''',
          '''View source code''': '''Ver el código fuente''',
          '''Look at the source code''': '''Ver el código fuente''',
          '''Changelog''': '''Historial de cambios''',
          '''View app changelog''':
              '''Ver el historial de cambios de la aplicación''',
          '''View License''': '''Ver la licencia''',
          '''Read software license''': '''Leer la licencia del software''',
          '''Third Party Licenses''': '''Licencias de terceras partes''',
          '''Read third party notices''':
              '''Leer las notas de terceras partes''',
        },
        'ru': {
          '''By %s''': '''От %s''',
          '''App developed by %s''': '''Приложение разработано %s''',
          '''Version:''': '''Версия:''',
          '''App version''': '''Версия приложения''',
          '''Report bugs''': '''Сообщить об ошибках''',
          '''Report bugs or request new feature''':
              '''Сообщить об ошибках или запросить новую функцию''',
          '''View source code''': '''Посмотреть исходный код''',
          '''Look at the source code''': '''Посмотри на исходный код''',
          '''Changelog''': '''Список изменений''',
          '''View app changelog''':
              '''Посмотреть список изменений приложения''',
          '''View License''': '''Посмотреть лицензию''',
          '''Read software license''': '''Прочитать лицензия приложения''',
          '''Third Party Licenses''': '''Сторонние лицензии''',
          '''Read third party notices''': '''Читать сторонние лицензии''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
