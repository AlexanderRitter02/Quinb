// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          '''Play''': '''Gioca''',
          '''Select game''': '''Seleziona gioco''',
          '''Rules''': '''Regole''',
          '''Settings''': '''Impostazioni''',
        },
        'es': {
          '''Play''': '''Jugar''',
          '''Select game''': '''Seleccionar juego''',
          '''Rules''': '''Reglas''',
          '''Settings''': '''Ajustes''',
          '''Sound games''': '''Juegos de sonidos''',
          '''Sound-based games enabled''': '''Juegos de sonidos habilitados''',
          '''Sound-based games disabled''':
              '''Juegos de sonidos deshabilitados''',
          '''players''': '''jugadores/as''',
          '''player''': '''jugador/a''',
          '''Enable volume?''': '''¿Habilitar volumen?''',
          '''\nIt looks like you have audio muted.\n\nYou should enable it in order to play sound-based games.\n''':
              '''\nParece que tienes el audio silenciado.\n\nDeberías habilitarlo para jugar los juegos basados en sonidos.\n''',
          '''Remember choice''': '''Recordar esta decisión''',
          '''Cancel''': '''Cancelar''',
          '''Enable''': '''Habilitar''',
        },
        'ru': {
          '''Play''': '''Играть''',
          '''Select game''': '''Выбери ''',
          '''Rules''': '''Правила''',
          '''Settings''': '''Настройки''',
          '''Sound games''': '''Звуки игры''',
          '''Sound-based games enabled''': '''Звуковые игры включены''',
          '''Sound-based games disabled''': '''Звуковые игры выключены''',
          '''players''': '''игроки''',
          '''player''': '''игрок''',
          '''Enable volume?''': '''Включить громкость?''',
          '''\nIt looks like you have audio muted.\n\nYou should enable it in order to play sound-based games.\n''':
              '''\nПохоже на то, что аудио приглушенно.\n\nТебе нужно включить его, чтобы играть в звуковые игры.\n''',
          '''Remember choice''': '''Запомните выбор''',
          '''Cancel''': '''Отмена''',
          '''Enable''': '''Включить''',
        },
      };
  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
