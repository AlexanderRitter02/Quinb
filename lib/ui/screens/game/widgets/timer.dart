import 'dart:async';
import 'dart:ui';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/util/local_data_controller.dart';

// Timer widget
class TimerWidget extends StatefulWidget {
  // Callback called when time runs out
  final VoidCallback onEnd;

  // When timer should pause
  final Function condition;

  // Game
  final BasicGame game;

  // Bar width
  static double width = 3;

  // Bar height
  static double height = 70;

  // Height decrement every second
  final double decrement = height / settings.timeAvailable;

  TimerWidget(this.game, {@required this.onEnd, @required this.condition});

  @override
  _TimerWidgetState createState() => _TimerWidgetState();
}

class _TimerWidgetState extends State<TimerWidget>
    with TickerProviderStateMixin {
  Timer timer;
  double height;

  @override
  void initState() {
    height = TimerWidget.height - (widget.game.elapsedTime * widget.decrement);
    timer = Timer.periodic(const Duration(seconds: 1), (_) {
      if (widget.condition()) return;
      setState(() {
        ++widget.game.elapsedTime;
        height =
            TimerWidget.height - (widget.game.elapsedTime * widget.decrement);
        if (widget.game.timeOver) widget.onEnd();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: Center(
        child: Container(
          height: height,
          width: TimerWidget.width,
          decoration: BoxDecoration(
            color: Colors.red,
            borderRadius: const BorderRadius.all(Radius.circular(40.0)),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }
}
