import 'package:quinb/models/game_category.dart';
import 'package:quinb/models/game_mode.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/util/local_data_controller.dart';

// Return a random game mode optionally based on category
GameMode getRandomMode([GameCategory category]) => (gameList
        .where((game) =>
            game.category == category ||
            (category == null &&
                (settings.enableSoundGames ||
                    game.category != GameCategory.Sound)))
        .toList()
          ..shuffle())
    .first
    .mode;
