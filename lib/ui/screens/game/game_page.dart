import 'dart:async';
import 'dart:ui';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/game_category.dart';
import 'package:quinb/models/game_mode.dart';
import 'package:quinb/models/game_phase.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/ui/screens/game/util/random_mode.dart';
import 'package:quinb/ui/screens/game/widgets/timer.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/i18n/game_page.i18n.dart';

// True if should change mode every round
bool randomMode;

class GamePage extends StatefulWidget {
  // Number of players
  final int playerCount;

  // Game mode
  final GameMode mode;

  // Game category
  final GameCategory category;

  GamePage({@required this.playerCount, this.mode, this.category}) {
    randomMode = (mode == null);
  }

  @override
  GamePageState createState() => GamePageState();
}

class GamePageState extends State<GamePage> with TickerProviderStateMixin {
  // Game in progress
  BasicGame _game;
  GameMode currentMode;

  @override
  void initState() {
    currentMode = widget.mode ?? (getRandomMode(widget.category));
    _game = currentMode.load(players: widget.playerCount, gamePageState: this);
    super.initState();
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this, value: 0);
    _animation =
        CurvedAnimation(parent: _animationController, curve: Curves.linear);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  if (_game.playerList.length > 1) playerZone(upZone: true),
                  _game.widget(),
                  playerZone(),
                ],
              ),
              if (_game.isMainPhase && _game.useTimer)
                Positioned(
                  top: widget.playerCount > 1
                      ? MediaQuery.of(context).size.height / 2 -
                          MediaQuery.of(context).padding.top -
                          MediaQuery.of(context).padding.bottom -
                          25
                      : 40,
                  right: 0,
                  child: TimerWidget(
                    _game,
                    condition: () =>
                        !_game.isMainPhase ||
                        _game.isPostPhase ||
                        _correct != null,
                    onEnd: () async {
                      _game.newPhase(GamePhase.postMain);
                      setState(() {});
                      popupController();
                      await Future.delayed(const Duration(seconds: 1));
                      if (mounted) {
                        setState(() {
                          _game.setInput();
                          if (randomMode) nextRandomGame();
                        });
                      }
                    },
                  ),
                ),
              resultPopup(),
            ],
          ),
        ),
        floatingActionButton:
            (_game.phaseLog.length <= 1 && settings.startPaused) ||
                    !_game.canStart() && _game.isPrePhase
                ? InkWell(
                    enableFeedback: false,
                    onTap: () {
                      if (_game.isPrePhase) {
                        _game.tapOnPrePhase();
                      } else if (_game.isMainPhase || _game.isPaused) {
                        _game.pause();
                      }
                    },
                    child: FloatingActionButton(
                      onPressed: null,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.play_arrow,
                          color: Theme.of(context).accentColor),
                    ))
                : null,
      ),
      onWillPop: exit,
    );
  }

  // Zone that contains optionList for one or two players
  Widget playerZone({bool upZone = false}) => Expanded(
        flex: 4,
        child: RotatedBox(
          quarterTurns: upZone ? 2 : 0,
          child: _game.phaseLog.length == 1 &&
                      (_game.isPaused || _game.isPrePhase) ||
                  _game.phaseLog.isEmpty
              ? Container(
                  color: const Color(0xFF155070),
                  child: SingleChildScrollView(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.fromLTRB(20, 25, 20, 75),
                      child: Text(
                        _game.info.rules,
                        textAlign: TextAlign.center,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                )
              : Container(
                  color: Colors.grey[300],
                  child: Row(
                    children: <Widget>[
                      Expanded(flex: 1, child: columnPlayer(upZone ? 1 : 0)),
                      if (_game.playerList.length > (upZone ? 2 : 3))
                        Expanded(flex: 1, child: columnPlayer(upZone ? 2 : 3)),
                    ],
                  ),
                ),
        ),
      );

  // Column that contains points and option for one player
  Widget columnPlayer(int playerIndex) => Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: 15,
            color: Colors.grey[200],
            child: Center(
              child: Text(
                  '${_game.playerList[playerIndex].points} / ${settings.requiredPoints} ' +
                      'points'.i18n),
            ),
          ),
          if ((_game.isMainPhase || _game.isOver || _game.isPostPhase) ||
              showExitOptions)
            ...optionList(playerIndex)
        ],
      );

  // Load a random game
  void nextRandomGame() {
    var _previousPlayerList = _game.playerList;
    var _oldRoundCount = _game.roundCount;
    currentMode = getRandomMode(widget.category);
    _game.dispose();
    _game = currentMode.load(players: widget.playerCount, gamePageState: this);
    _game.playerList = _previousPlayerList;
    _game.roundCount = _oldRoundCount;
  }

  // Last input
  Option lastInput;
  int lastPlayer;

  // List of possible game options and player points
  List<Widget> optionList(int playerIndex) {
    return <Widget>[
      if (!_game.isOver && !showExitOptions)
        for (var option in _game.optionsAvailable)
          Expanded(
            child: InkWell(
              enableFeedback: _game.enableFeedback,
              onTap: () async {
                if (!_game.isMainPhase ||
                    _game.isPostPhase ||
                    _correct != null) {
                  return;
                }
                _game.newPhase(GamePhase.postMain);
                setState(() {
                  lastInput = option;
                  lastPlayer = playerIndex;
                  _correct = _game.validateInput(option.value);
                });
                _resultRotated = [1, 2].contains(playerIndex);
                await _animationController.forward().whenComplete(() async {
                  await Future.delayed(const Duration(milliseconds: 100));
                  _animationController.reverse();
                });
                await Future.delayed(const Duration(milliseconds: 1500));
                _game?.setInput(correct: _correct, playerIndex: playerIndex);
                if (randomMode && !(_game?.isOver ?? true)) nextRandomGame();
                _correct = null;
              },
              child: Container(
                decoration: BoxDecoration(
                    color: option.color ?? _game.playerList[playerIndex].color,
                    border: widget.playerCount > 2 && playerIndex <= 1
                        ? const Border(
                            right: BorderSide(color: Colors.white, width: 0.4))
                        : null),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          option.text,
                          style: const TextStyle(
                              fontSize: 22, color: Colors.white),
                        ),
                      ),
                    ),
                    if (_game.optionsAvailable.length > 1)
                      Container(
                        margin: const EdgeInsets.only(right: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Center(
                              child: (lastPlayer == playerIndex &&
                                      !(_correct ?? true) &&
                                      (lastInput == option))
                                  ? Icon(Icons.close, color: Colors.white)
                                  : (lastPlayer == playerIndex &&
                                          _correct != null &&
                                          (_game.validateInput(option.value)))
                                      ? Icon(Icons.done, color: Colors.white)
                                      : (_game.timeOver ||
                                                  (lastPlayer != playerIndex &&
                                                      _correct != null)) &&
                                              _game.validateInput(option.value)
                                          ? Icon(Icons.arrow_left,
                                              color: Colors.white)
                                          : Container(),
                            ),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
            ),
          )
      else if ((playerIndex == _game.playerList.indexOf(_game.ranking.first) &&
              !showExitOptions) ||
          (playerIndex == 0 && showExitOptions))
        for (var option in <Option>[
          Option(-1, text: 'Resume'.i18n),
          Option(0, text: 'Restart'.i18n),
          Option(1, text: 'Exit'.i18n),
        ])
          if (option.value != -1 || showExitOptions)
            Expanded(
              child: InkWell(
                onTap: () {
                  if (option.value == 0) {
                    setState(() {
                      currentMode =
                          widget.mode ?? getRandomMode(widget.category);
                      _game = currentMode.load(
                          players: widget.playerCount, gamePageState: this);
                      showExitOptions = false;
                    });
                  } else if (option.value == -1) {
                    showExitOptions = false;
                    _game.pause();
                  } else {
                    _game.dispose();
                    Navigator.pop(context);
                  }
                },
                child: Container(
                  color: _game.playerList[playerIndex].color,
                  child: Center(
                    child: Text(
                      option.text,
                      style: const TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  ),
                ),
              ),
            )
          else if (option.value != -1)
            Expanded(child: Container(color: Colors.grey[200]))
    ];
  }

  // Manage back button
  bool showExitOptions = false;
  Future<bool> exit() {
    if (_game.isPrePhase ||
        _game.isOver ||
        _game.isStarting ||
        showExitOptions ||
        (!randomMode && _game.phaseLog.length < 5) ||
        (randomMode && _game.phaseLog.length == 1 && _game.isPaused)) {
      _game.dispose();
      return Future.value(true);
    } else if (_game.isMainPhase || _game.isPaused) {
      setState(() {
        _game.pause();
        showExitOptions = _game.isPaused;
      });
    }
    return Future.value(false);
  }

  // Variables used by the game popup
  AnimationController _animationController;
  Animation<double> _animation;
  bool _correct;
  bool _resultRotated = false;

  // Answer popup controller
  void popupController() {
    _animationController.forward().whenComplete(() async {
      await Future.delayed(const Duration(milliseconds: 100));
      if (!_animationController.toStringDetails().contains('DISPOSED')) {
        _animationController.reverse();
      }
    });
  }

  // Popup displayed after players input
  // Green/Red/White popup (right answer/wrong answer/time over)
  Widget resultPopup() => Positioned(
        right: MediaQuery.of(context).size.width / 2 - 100,
        bottom: _correct == null ? null : (_resultRotated ? null : 50),
        top: _correct == null
            ? MediaQuery.of(context).size.height / 2
            : (_resultRotated ? 50 : null),
        child: ScaleTransition(
          scale: _animation,
          child: RotatedBox(
            quarterTurns: _resultRotated ? 2 : 0,
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: (_correct == null)
                      ? Colors.white
                      : (_correct ? Colors.green : Colors.red),
                  border: Border.all(color: Colors.white, width: 5)),
              width: 200.0,
              height: 200.0,
              child: Icon(
                (_correct == null)
                    ? Icons.timer_off
                    : (_correct ? Icons.check : Icons.clear),
                size: 150,
                color: _correct == null ? Colors.black : Colors.white,
              ),
            ),
          ),
        ),
      );

  // Set state used by the game variable
  void refresh() {
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    _animationController.dispose();
    _game.dispose();
    _game = null;
    super.dispose();
  }
}
