import 'package:flutter/material.dart';

import 'package:quinb/models/settings.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/ui/screens/settings/settings_page.dart';
import 'package:quinb/i18n/restore_dialog.i18n.dart';

// Dialog used to restore default settings
Future restoreSettingsDialog(SettingsPageState settingsPageState) {
  return showDialog(
    context: settingsPageState.context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Restore default settings?'.i18n),
        content: Text(
            'Are you sure you want to delete your settings and restore default ones?'
                .i18n),
        actions: <Widget>[
          FlatButton(
            child: Text('Restore'.i18n),
            onPressed: () {
              settings = Settings({
                'firstRun': false,
                'enableSoundGames': settings.enableSoundGames,
              });
              saveSettings();
              settingsPageState.refresh();
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text('Cancel'.i18n),
            onPressed: Navigator.of(context).pop,
          ),
        ],
      );
    },
  );
}
