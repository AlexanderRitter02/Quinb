import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/models/game_category.dart';
import 'package:quinb/models/game_detail.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/ui/basic.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/ui/screens/info/info_page.dart';
import 'package:quinb/ui/screens/rules/rules_page.dart';
import 'package:quinb/ui/screens/select/widgets/fab.dart';
import 'package:quinb/ui/screens/settings/settings_page.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/i18n/select_page.i18n.dart';

class SelectPage extends StatefulWidget {
  @override
  _SelectPageState createState() => _SelectPageState();
}

class _SelectPageState extends State<SelectPage> {
  // Category shown (0 -> All)
  int _selectedCategory = 0;

  // List of games in the selected category
  List<GameDetails> get _gameList => (_selectedCategory > 0)
      ? gameList
          .where(
              (p) => p.category == GameCategory.values[_selectedCategory - 1])
          .toList()
      : List<GameDetails>.from(gameList);

  // Key of the scaffold of this page
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: appBar(),
      body: GestureDetector(
          onHorizontalDragEnd: (DragEndDetails details) {
            setState(() {
              _selectedCategory +=
                  (details.velocity.pixelsPerSecond.dx > 0) ? -1 : 1;
              if (!settings.enableSoundGames &&
                  _selectedCategory ==
                      GameCategory.values.indexOf(GameCategory.Sound) + 1) {
                _selectedCategory +=
                    (details.velocity.pixelsPerSecond.dx > 0) ? -1 : 1;
              }
              _selectedCategory = min(max(_selectedCategory, 0), 3);
            });
          },
          child: ListView.separated(
              padding: const EdgeInsets.fromLTRB(0, 8, 0, 35),
              itemCount: _gameList.length,
              itemBuilder: (BuildContext context, int index) {
                return Visibility(
                  visible: (settings.enableSoundGames ||
                      _gameList[index].category != GameCategory.Sound),
                  child: ListTile(
                    title: Text(
                      _gameList[index].title,
                      style: const TextStyle(fontSize: 20),
                    ),
                    subtitle: Text(_gameList[index].subtitle),
                    leading: Icon(_gameList[index].category.icon),
                    isThreeLine: true,
                    onTap: () {
                      Navigator.push(
                        context,
                        FadeRoute(GamePage(
                            playerCount: settings.players,
                            mode: _gameList[index].mode)),
                      );
                    },
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) => Visibility(
                  visible: settings.enableSoundGames ||
                      _gameList[index].category != GameCategory.Sound,
                  child: Divider(
                      height: 1, color: Colors.grey.withOpacity(0.4))))),
      bottomNavigationBar: bottomBar(),
      floatingActionButton: Fab(_selectedCategory),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }

  // Home app bar
  Widget appBar() {
    return AppBar(
      elevation: 0,
      title: Text('GAMES'.i18n,
          style: TextStyle(color: Theme.of(context).primaryColor)),
      centerTitle: true,
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(52),
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom:
                  BorderSide(color: Theme.of(context).primaryColor, width: 3),
            ),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(Icons.help_outline),
                tooltip: 'Rules'.i18n,
                onPressed: () {
                  Navigator.push(context, FadeRoute(RulesPage()));
                },
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(settings.enableSoundGames
                    ? Icons.volume_up
                    : Icons.volume_off),
                tooltip: 'Sound games'.i18n,
                onPressed: () {
                  setState(() {
                    _selectedCategory = 0;
                    settings.enableSoundGames = !settings.enableSoundGames;
                    saveSettings();
                    volumeSnackBar(
                      _scaffoldKey,
                      floating: true,
                      text: settings.enableSoundGames
                          ? 'Sound-based games enabled'.i18n
                          : 'Sound-based games disabled'.i18n,
                    );
                  });
                },
              ),
              Expanded(
                child: Center(
                  child: DropdownButton<int>(
                    value: settings.players,
                    underline: Container(),
                    icon: Container(),
                    elevation: 0,
                    onChanged: (int newValue) {
                      setState(() {
                        settings.players = newValue;
                        saveSettings();
                      });
                    },
                    items: [1, 2, 3, 4]
                        .map<DropdownMenuItem<int>>(
                          (int value) => DropdownMenuItem<int>(
                            value: value,
                            child: Text(
                              '$value ' +
                                  (value > 1 ? 'players'.i18n : 'player'.i18n),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ),
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(Icons.settings),
                tooltip: 'Settings'.i18n,
                onPressed: () {
                  Navigator.push(context, FadeRoute(SettingsPage()));
                },
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(Icons.info_outline),
                tooltip: 'Info'.i18n,
                onPressed: () {
                  Navigator.push(context, FadeRoute(InfoPage()));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Home bottom bar
  Widget bottomBar() {
    return BottomAppBar(
      child: Container(
        height: 56,
        decoration: BoxDecoration(
            border: Border(
                top: BorderSide(
                    color: Theme.of(context).primaryColor, width: 3))),
        child: Container(
          margin: const EdgeInsets.only(right: 75),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              for (int i = -1; i < GameCategory.values.length; i++)
                if (settings.enableSoundGames ||
                    (i == -1 || GameCategory.values[i] != GameCategory.Sound))
                  Flexible(
                    flex: (_selectedCategory != i + 1) ? 1 : 2,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _selectedCategory = i + 1;
                        });
                      },
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Center(
                        child: (_selectedCategory != i + 1)
                            ? Icon(
                                i == -1
                                    ? Icons.scatter_plot
                                    : GameCategory.values[i].icon,
                                color: Colors.black87,
                              )
                            : Text(
                                i == -1
                                    ? 'All'.i18n
                                    : GameCategory.values[i].name,
                                maxLines: 1,
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w900,
                                  color: Theme.of(context)
                                      .primaryColor
                                      .withOpacity(0.7),
                                ),
                              ),
                      ),
                    ),
                  ),
            ],
          ),
        ),
      ),
    );
  }
}
