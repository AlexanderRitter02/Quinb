import 'package:flutter/material.dart';

import 'package:quinb/models/game_category.dart';
import 'package:quinb/ui/basic.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/i18n/fab.i18n.dart';

// Floating action button that start random games
class Fab extends StatelessWidget {
  final int selectedCategoryIndex;
  Fab(this.selectedCategoryIndex);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      elevation: 0,
      backgroundColor: Theme.of(context).primaryColor,
      tooltip: 'Random'.i18n,
      child: Icon(Icons.shuffle),
      onPressed: () {
        Navigator.push(
          context,
          FadeRoute(
            GamePage(
              playerCount: settings.players,
              category: selectedCategoryIndex != 0
                  ? GameCategory.values[selectedCategoryIndex - 1]
                  : null,
            ),
          ),
        );
      },
    );
  }
}
