import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:quinb/ui/screens/info/widgets/license_dialog.dart';
import 'package:quinb/i18n/info_page.i18n.dart';

class InfoPage extends StatefulWidget {
  @override
  _InfoPageState createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {
  static final _appVersion = '1.0.0';

  // List of menu items displayed in the info page
  final infoMenuList = <Map<String, dynamic>>[
    {
      'title': 'By %s'.i18n.fill(['DeepDaikon']),
      'subtitle': 'App developed by %s'.i18n.fill(['DeepDaikon']),
      'url': 'https://deepdaikon.xyz',
      'icon': const Icon(Icons.change_history),
    },
    {
      'title': 'Version:'.i18n + ' $_appVersion',
      'subtitle': 'App version'.i18n,
      'icon': const Icon(Icons.looks_one),
    },
    /*{
      'title': 'Updates',
      'subtitle': 'Search for updates',
      'url': 'https://f-droid.org/packages/xyz.deepdaikon.quinb/',
      'icon': const Icon(Icons.system_update),
    },*/
    {
      'title': 'Report bugs'.i18n,
      'subtitle': 'Report bugs or request new feature'.i18n,
      'url': 'https://gitlab.com/DeepDaikon/Quinb/issues',
      'icon': const Icon(Icons.bug_report),
    },
    {
      'title': 'View source code'.i18n,
      'subtitle': 'Look at the source code'.i18n,
      'url': 'https://gitlab.com/DeepDaikon/Quinb',
      'icon': const Icon(Icons.developer_mode),
    },
    {
      'title': 'Changelog'.i18n,
      'subtitle': 'View app changelog'.i18n,
      'url': 'https://gitlab.com/DeepDaikon/Quinb/blob/master/CHANGELOG',
      'icon': const Icon(Icons.playlist_add),
    },
    {
      'title': 'View License'.i18n,
      'subtitle': 'Read software license'.i18n,
      'url': 'https://gitlab.com/DeepDaikon/Quinb/blob/master/LICENSE',
      'icon': const Icon(Icons.chrome_reader_mode),
    },
    {
      'title': 'Third Party Licenses'.i18n,
      'subtitle': 'Read third party notices'.i18n,
      'icon': const Icon(Icons.code),
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('INFO'), centerTitle: true),
      body: ListView.builder(
        padding: const EdgeInsets.all(8.0),
        itemCount: infoMenuList.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            leading: Icon(infoMenuList[index]['icon'].icon, size: 27),
            title: Text(
              infoMenuList[index]['title'],
              style: const TextStyle(fontSize: 20),
            ),
            subtitle: Text(infoMenuList[index]['subtitle']),
            onTap: () {
              if (infoMenuList[index].containsKey('url')) {
                launch('${infoMenuList[index]['url']}');
              } else if (infoMenuList[index]['title'] ==
                  'Third Party Licenses'.i18n) {
                licenseDialog(context);
              }
            },
          );
        },
      ),
    );
  }
}
