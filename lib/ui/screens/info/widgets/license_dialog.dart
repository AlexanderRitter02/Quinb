import 'package:flutter/material.dart';

import 'package:quinb/ui/screens/info/resources/third_party_licenses.dart';
import 'package:quinb/i18n/license_dialog.i18n.dart';

// Display third party licenses in an alert dialog
Future licenseDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Third Party Licenses'.i18n),
          content: Container(
            width: double.maxFinite,
            child: ListView.builder(
              itemCount: licenses.length,
              itemBuilder: (BuildContext context, int index) {
                return ExpansionTile(
                  title: Text(licenses[index]['lib']),
                  initiallyExpanded: true,
                  children: <Widget>[
                    SingleChildScrollView(child: Text(licenses[index]['text'])),
                  ],
                );
              },
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: const Text('Ok'),
              onPressed: Navigator.of(context).pop,
            ),
          ],
        );
      },
    );
