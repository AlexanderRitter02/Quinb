import 'package:flutter/material.dart';

// Game rule
class Rule {
  // Rule title
  final String title;

  // Rule description
  final String subtitle;

  // Rule image if white theme
  final String image;

  // Rule image if dark theme
  final String lightImage;

  // Rule icon (instead of image)
  final IconData icon;
  final double iconSize;

  Rule({
    @required this.title,
    @required this.subtitle,
    this.image,
    this.lightImage,
    this.icon,
    this.iconSize = 150,
  });
}
