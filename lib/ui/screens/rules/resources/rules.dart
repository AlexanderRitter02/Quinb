import 'package:flutter/material.dart';

import 'package:quinb/ui/screens/rules/util/rule.dart';
import 'package:quinb/i18n/rules.i18n.dart';

// Global rules
List<Rule> rules() => [
      Rule(
        title: 'Q u i n b',
        subtitle:
            'Quinb is a multiplayer reaction game.\nTrain your mind and your reflexes while having fun!\n\nSwipe to right to learn how to play.'
                .i18n,
        image: 'assets/graphics/icon_black.png',
      ),
      Rule(
        title: 'Games'.i18n,
        subtitle:
            'Quinb contains many minigames based on 3 different categories:\nlogic, audio, vibration.'
                .i18n,
        icon: Icons.games,
      ),
      Rule(
        title: 'Points'.i18n,
        subtitle:
            'In every game you have to answer questions as fast as you can.\nIf the answer is correct you score a point, otherwise you lose one.\n'
                .i18n,
        icon: Icons.exposure_plus_1,
      ),
      Rule(
        title: 'Goal'.i18n,
        subtitle:
            'Each match consists of a succession of different minigames.\nThe goal is to score 7 points before your opponents.'
                .i18n,
        icon: Icons.beenhere,
      ),
      Rule(
        title: 'Multiplayer'.i18n,
        subtitle:
            'Quinb can be played by 4 players on the same device, but you can also play alone if you want.'
                .i18n,
        icon: Icons.people,
      ),
      Rule(
        title: "That's all".i18n,
        subtitle:
            'You will learn the rules of each minigame by playing.\n\nKeep in mind that this game is way more fun if played with friends!'
                .i18n,
        icon: Icons.done,
        iconSize: 210,
      ),
    ];
