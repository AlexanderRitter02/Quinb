import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/ui/basic.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/ui/screens/home/widgets/page_button.dart';
import 'package:quinb/ui/screens/info/info_page.dart';
import 'package:quinb/ui/screens/rules/rules_page.dart';
import 'package:quinb/ui/screens/select/select_page.dart';
import 'package:quinb/ui/screens/settings/settings_page.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/util/volume_controller.dart';
import 'package:quinb/i18n/home_page.i18n.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  List<Map<String, dynamic>> pageList() {
    return [
      {
        'title': 'Play'.i18n,
        'goto': () => GamePage(playerCount: settings.players)
      },
      {'title': 'Select game'.i18n, 'goto': () => SelectPage()},
      {'title': 'Rules'.i18n, 'goto': () => RulesPage(null)},
      {'title': 'Settings'.i18n, 'goto': () => SettingsPage()},
    ];
  }

  // Title rotation animation
  AnimationController rotationController;
  Animation<double> animation;

  // Key of the scaffold of this page
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    rotationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    animation = Tween<double>(begin: 0, end: pi).animate(rotationController);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (settings.enableVolumeOnStart == null &&
          currentVol == 0 &&
          settings.enableSoundGames) {
        await volumeDialog();
      } else if (currentVol == 0 && settings.enableSoundGames) {
        settings.enableVolumeOnStart
            ? await setVol(maxVol ~/ 2)
            : volumeSnackBar(_scaffoldKey);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: const BoxDecoration(gradient: appGradient),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Center(
                child: InkWell(
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onTap: () {
                    setState(() {
                      rotationController.isCompleted
                          ? rotationController.reverse()
                          : rotationController.forward();
                    });
                  },
                  child: AnimatedBuilder(
                    animation: animation,
                    child: Text(
                      '¿ quinb ?',
                      style: TextStyle(
                        letterSpacing: 5,
                        color: Colors.white,
                        fontSize: 56,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                    builder: (context, child) {
                      return Container(
                        child: Transform.rotate(
                          angle: animation.value,
                          child: Container(
                            child: child,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
            for (var page in pageList())
              PageButton(
                title: page['title'],
                onPressed: () {
                  Navigator.push(context, FadeRoute(page['goto']()));
                },
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                      settings.enableSoundGames
                          ? Icons.volume_up
                          : Icons.volume_off,
                      color: Colors.white.withOpacity(0.7),
                      size: 28),
                  tooltip: 'Sound games'.i18n,
                  onPressed: () {
                    setState(() {
                      settings.enableSoundGames = !settings.enableSoundGames;
                      saveSettings();
                      volumeSnackBar(
                        _scaffoldKey,
                        text: settings.enableSoundGames
                            ? 'Sound-based games enabled'.i18n
                            : 'Sound-based games disabled'.i18n,
                      );
                    });
                  },
                ),
                DropdownButton<int>(
                  value: settings.players,
                  underline: Container(),
                  selectedItemBuilder: (BuildContext context) {
                    return [1, 2, 3, 4]
                        .map<Widget>((int value) => Center(
                              child: Text(
                                '$value ' +
                                    (value > 1
                                        ? 'players'.i18n
                                        : 'player'.i18n),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ))
                        .toList();
                  },
                  icon: Container(),
                  elevation: 0,
                  onChanged: (int newValue) {
                    setState(() {
                      settings.players = newValue;
                      saveSettings();
                    });
                  },
                  items: [1, 2, 3, 4]
                      .map<DropdownMenuItem<int>>(
                        (int value) => DropdownMenuItem<int>(
                          value: value,
                          child: Text(
                            '$value ' +
                                (value > 1 ? 'players'.i18n : 'player'.i18n),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: value == settings.players
                                  ? FontWeight.bold
                                  : FontWeight.normal,
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
                IconButton(
                  icon: Icon(Icons.info_outline,
                      color: Colors.white.withOpacity(0.7), size: 28),
                  onPressed: () {
                    Navigator.push(context, FadeRoute(InfoPage()));
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  // Dialog displayed if volume == 0
  Future<void> volumeDialog() {
    var rememberChoice = false;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return WillPopScope(
              onWillPop: () {
                volumeSnackBar(_scaffoldKey);
                return Future.value(true);
              },
              child: AlertDialog(
                title: Text(
                  'Enable volume?'.i18n,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Theme.of(context).accentColor),
                ),
                content: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text(
                          '\nIt looks like you have audio muted.\n\nYou should enable it in order to play sound-based games.\n'
                              .i18n,
                          style: const TextStyle(fontSize: 18)),
                      InkWell(
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Row(
                            children: <Widget>[
                              Checkbox(
                                  value: rememberChoice,
                                  onChanged: (newValue) {
                                    setState(() {
                                      rememberChoice = newValue;
                                    });
                                  }),
                              Text('Remember choice'.i18n),
                            ],
                          ),
                          onTap: () {
                            setState(() {
                              rememberChoice = !rememberChoice;
                            });
                          }),
                    ],
                  ),
                ),
                elevation: 0,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancel'.i18n),
                    onPressed: () {
                      if (rememberChoice) {
                        settings.enableVolumeOnStart = false;
                        saveSettings();
                      }
                      volumeSnackBar(_scaffoldKey);
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('Enable'.i18n),
                    onPressed: () {
                      setVol(maxVol ~/ 2);
                      if (rememberChoice) {
                        settings.enableVolumeOnStart = true;
                        saveSettings();
                      }
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            );
          });
        });
  }
}
