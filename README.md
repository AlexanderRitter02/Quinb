# Quinb

Quinb is a multiplayer reaction game: train your mind and your reflexes while having fun.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/xyz.deepdaikon.quinb/)

## Description

Quinb is a reaction / logic game for up to 4 players on the same device.

It contains many minigames where you have to answer questions as fast as you can to get a point.

These games are based on 3 different categories:

* Logic: games that require intuition, logic and fast reflexes
* Audio: sound-based games, you have to listen carefully to find out the correct answer
* Vibration: vibration-based games that require you to listen carefully to the vibrations of your device

You can play alone if you want, but it is more fun to play with friends of all ages. It's great if you are stuck with nothing to do while you're with friends.

If you think you're fast, challenge and beat your friends!

## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/HomePage.png" height="320">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/Screen_1.png" height="320">
<img src ="fastlane/metadata/android/en-US/images/phoneScreenshots/Screen_2.png" height="320">

## Translations

If you want to translate this app, first of all thank you :)

0. Clone this project (`git clone https://gitlab.com/DeepDaikon/Quinb.git`)

1. If your language is not in the locales directory (e.g. if does not exist the `locales/de` directory), copy-paste the `locales/template` directory and rename it to your language code.

2. Translate each .po file in `locales/YOUR_LOCALE/LC_MESSAGES/`.

You just have to edit the `msgstr` strings in each .po file. [Poedit](https://poedit.net/) can be used to make this step easier.

3. When you are ready open a merge request.

### Testing your translations

If you want to test your translations by compiling the app:

0. Follow the [how to build guide](https://gitlab.com/DeepDaikon/Quinb#building).

1. Run `flutter pub get`.

2. Run `flutter pub run quinb:update_translation`.

This command read your .po files and update the .dart ones. It has to be executed every time you update a .po file and want to test the new translations.

3. Add your language code to the supported locales, if missing. To do this open `Quinb/lib/main.dart` and add your locale to the supportedLocales list. It should be a line like this: `Locale('de')`.

4. Reload or rebuild the app and test it.

5. When you are ready open a merge request. You don't need to include the auto-generated files inside `lib/i18n` in your merge request.

## Building

0. Clone this project (`git clone https://gitlab.com/DeepDaikon/Quinb.git`)

1. To build this app you need the Flutter SDK and the Android SDK. If you already have them go to step 2.

The official installation guide about this can be found here: https://flutter.dev/docs/get-started/install. It is well explained but if you have problems open an issue so that I can help you.

You can check that everything is fine by running `flutter doctor`.

2. Run `flutter version x.y.z` (replace x.y.z with the flutter version specified in the [`pubspec.yaml` file](https://gitlab.com/DeepDaikon/Quinb/-/blob/master/pubspec.yaml#L7). E.g. `flutter version 1.17.3`).

Run `flutter --version` to check that you are on the right version.

3. Open `Quinb/android/app/build.gradle` and remove `signingConfigs` and `buildTypes`

You have to remove these lines:

```
signingConfigs {
     release {
          keyAlias keystoreProperties['keyAlias']
          keyPassword keystoreProperties['keyPassword']
          storeFile file(keystoreProperties['storeFile'])
          storePassword keystoreProperties['storePassword']
     }
}

buildTypes {
     release {
          signingConfig signingConfigs.debug
     }
}
```

4. Open the Quinb directory and build the app by running `flutter build apk`

Generated apks are placed in `Quinb/build/app/outputs/apk`.

If you have Quinb on your device installed from F-Droid, you have to uninstall it before installing the new apk.

If you want to test your code or your translations you can use `flutter run` which allows you to reload the code of a live running app without restarting it. Use flutter run and then hot reload by pressing 'r'. If you have configured an IDE like Android Studio you can use the hot reload by clicking on [its icon](https://flutter.dev/assets/tools/android-studio/main-toolbar-8f8e4ce1c7a5ec70c4a288837aa3ea083fe03deef28d99567a391ef005a59c3f.png).
