# German translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.
# Alexander Ritter <https://github.com/AlexanderRitter02>, 2020.
#
msgid ""
msgstr ""
#: lib/ui/screens/info/info_page.dart:
msgid "By %s"
msgstr "Von %s"

#: lib/ui/screens/info/info_page.dart:
msgid "App developed by %s"
msgstr "App entwickelt von %s"

#: lib/ui/screens/info/info_page.dart:
msgid "Version:"
msgstr "Version:"

#: lib/ui/screens/info/info_page.dart:
msgid "App version"
msgstr "App-Version"

#: lib/ui/screens/info/info_page.dart:
msgid "Report bugs"
msgstr "Fehler melden"

#: lib/ui/screens/info/info_page.dart:
msgid "Report bugs or request new feature"
msgstr "Melde Fehler oder schlage eine neue Funktion vor"

#: lib/ui/screens/info/info_page.dart:
msgid "View source code"
msgstr "Quellcode anzeigen"

#: lib/ui/screens/info/info_page.dart:
msgid "Look at the source code"
msgstr "Schau dir den Quellcode an"

#: lib/ui/screens/info/info_page.dart:
msgid "Changelog"
msgstr "Änderungsprotokoll"

#: lib/ui/screens/info/info_page.dart:
msgid "View app changelog"
msgstr "Änderungsprotokoll anzeigen"

#: lib/ui/screens/info/info_page.dart:
msgid "View License"
msgstr "Lizenz anzeigen"

#: lib/ui/screens/info/info_page.dart:
msgid "Read software license"
msgstr "Software-Lizenz lesen"

#: lib/ui/screens/info/info_page.dart:
msgid "Third Party Licenses"
msgstr "Drittanbieterlizenzen"

#: lib/ui/screens/info/info_page.dart:
msgid "Read third party notices"
msgstr "Hinweise von Drittanbietern lesen"
