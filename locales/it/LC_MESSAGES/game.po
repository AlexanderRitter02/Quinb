# Italian translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.
# DeepDaikon <deepdaikon@tuta.io>, 2020.
#
msgid ""
msgstr ""
#: lib/ui/screens/game/game_page.dart:
msgid "points"
msgstr "punti"

#: lib/ui/screens/game/game_page.dart:
msgid "Resume"
msgstr "Riprendi"

#: lib/ui/screens/game/game_page.dart:
msgid "Restart"
msgstr ""

#: lib/ui/screens/game/game_page.dart:
msgid "Exit"
msgstr ""
