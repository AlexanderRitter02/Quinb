# Italian translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.
# DeepDaikon <deepdaikon@tuta.io>, 2020.
#
msgid ""
msgstr ""
#: lib/resources/game_list.dart:
msgid "Remember"
msgstr "Ricorda"

#: lib/resources/game_list.dart:
msgid "Remember the colors and letters that appear"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Remember the sequence of colors and letters that will appear on the screen"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Three figures"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Find three identical figures"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Tap when there are 3 identical figures"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Tap the screen when you see at least 3 figures that have the same color and the same letter inside"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Vibration repetitions"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Count how many times you hear the vibration"
msgstr ""

#: lib/resources/game_list.dart:
msgid "How many times have you heard the vibration?"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Count the lights"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Count how many lights come on"
msgstr ""

#: lib/resources/game_list.dart:
msgid "How many light have come on?"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Some lights are about to come on in rapid succession.\n\nCounts how many lights come on."
msgstr ""

#: lib/resources/game_list.dart:
msgid "Sound repetitions"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Count how many times you hear the sound"
msgstr ""

#: lib/resources/game_list.dart:
msgid "How many times have you heard the sound?"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Shortest vibration"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Find the shortest vibration"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Which one was the shortest vibration?"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Try to figure out which one is the shortest vibration among four"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Math calculation"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Solve the equations"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Solve the equations in your head as fast as you can"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Lights on"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Count if there are more lights on than off"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Are there more lights on than off?"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Tap the screen if there are more yellow squares than black ones"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Vibration sequence"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Remember the sequence you hear"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Which sequence was it?"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Listen to the vibration sequence and remember it.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Mix Colors"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Mix the colors"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Try to figure out which color results from the color mixing"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Audio sequence"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Listen to the sounds sequence and remember it"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Reflexes"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Tap at the right moment"
msgstr ""

#: lib/resources/game_list.dart:
msgid "As soon as the screen becomes colored, tap the option of the same color.\n\nKeep in mind that you have little time before the screen turns white again."
msgstr ""

#: lib/resources/game_list.dart:
msgid "Fastest object"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Find the fastest object"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Try to figure out which one is the fastest moving object"
msgstr ""

#: lib/resources/game_list.dart:
msgid "What color is it"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Recognize which color is used"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Answer as fast as you can which color is written or which color is used for the background or for the text"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Listen and calculate"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Add or subtract based on what you hear"
msgstr ""

#: lib/resources/game_list.dart:
msgid "What is the result?"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Sum if you hear a duck, subtract if you hear a cat.\n\nThe amount of the sum and the subtraction will be shown during the game."
msgstr ""

#: lib/resources/game_list.dart:
msgid "Recognize the sound"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Tap the screen when you hear the sound"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Listen to the sample sound twice here.\n\nThen tap the screen when you hear it during a sequence of sounds."
msgstr ""

#: lib/resources/game_list.dart:
msgid "Hear and calculate"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Add or subtract based on vibrations"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Sum if you hear a long vibration, subtract if you hear a short one.\n\nThe amount of the sum and the subtraction will be shown during the game.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Animal farm"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Tap when you hear the correct animal"
msgstr ""

#: lib/resources/game_list.dart:
msgid "Then tap the screen when you hear the correct animal during a sequence of sounds"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Quinb is a multiplayer reaction game.\nTrain your mind and your reflexes while having fun!\n\nSwipe to right to learn how to play."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Games"
msgstr "Giochi"

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Quinb contains many minigames based on 3 different categories:\nlogic, audio, vibration."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Points"
msgstr "Punti"

#: lib/ui/screens/rules/resources/rules.dart:
msgid "In every game you have to answer questions as fast as you can.\nIf the answer is correct you score a point, otherwise you lose one.\n"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Goal"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Each match consists of a succession of different minigames.\nThe goal is to score 7 points before your opponents."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Multiplayer"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Quinb can be played by 4 players on the same device, but you can also play alone if you want."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "That's all"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "You will learn the rules of each minigame by playing.\n\nKeep in mind that this game is way more fun if played with friends!"
msgstr ""
