# INSERT LANGUAGE HERE translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
#: lib/ui/screens/game/game_page.dart:
msgid "points"
msgstr ""

#: lib/ui/screens/game/game_page.dart:
msgid "Resume"
msgstr ""

#: lib/ui/screens/game/game_page.dart:
msgid "Restart"
msgstr ""

#: lib/ui/screens/game/game_page.dart:
msgid "Exit"
msgstr ""
