// ignore_for_file: avoid_print
import 'dart:io';

// Read each .po file inside locales and re-generate dart files inside lib/i18n
// Use this command to run the script: flutter pub run quinb:update_translations
void main() {
  var localesDir = Directory('locales');
  var exp = RegExp(r'#: (.*):\nmsgid "(.*)"\nmsgstr "(.*)"');
  // i18nFiles -> fileName : <lang : list of translations>
  var i18nFiles = <String, Map<String, List<Translation>>>{};
  localesDir.listSync().forEach((locale) {
    var lang = locale.path.split('/').last;
    if (lang == 'template') return;
    var messagesDir = Directory(locale.path + '/LC_MESSAGES');
    messagesDir.listSync().forEach((poFile) {
      if (poFile is File) {
        var content = poFile.readAsStringSync();
        content = content.replaceAll('"\n"', ''); // Remove multiline strings
        exp.allMatches(content).forEach((match) {
          var fileName = match.group(1);
          var msgid = match.group(2);
          var msgstr = match.group(3);
          if (msgstr.isEmpty) return;
          var translation = Translation(msgid, msgstr);
          if (i18nFiles.containsKey(fileName)) {
            i18nFiles[fileName].containsKey(lang)
                ? i18nFiles[fileName][lang].add(translation)
                : i18nFiles[fileName][lang] = [translation];
          } else {
            i18nFiles[fileName] = {
              lang: [translation]
            };
          }
        });
      }
    });
  });
  Directory('lib/i18n').listSync().forEach((f) {
    f.delete(recursive: true);
  });
  i18nFiles.forEach((_fileName, _languageTranslationsMap) {
    var i18nFileName = RegExp(r'.*\/(.*).dart').firstMatch(_fileName).group(1);
    var newI18nFile = File('lib/i18n/$i18nFileName.i18n.dart')..createSync();
    newI18nFile.writeAsStringSync("""
// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';
extension Localization on String {
  static final _t = Translations.byLocale('en_us') + {
    ${() {
      var languagesAndTranslations = '';
      _languageTranslationsMap.forEach((_language, _translations) {
        languagesAndTranslations += """
          '$_language': {
            ${_translations.fold('', (previousValue, element) {
          return previousValue +
              """'''${element.msgid}''':'''${element.msgstr}''',""";
        })}
        },""";
      });
      return languagesAndTranslations;
    }()}
};String get i18n => localize(this, _t);
String fill(List<Object> params) => localizeFill(this, params);}""");
  });
  print('Translations successfully updated!');
  print('Formatting code...');
  try {
    Process.runSync('flutter', ['format', 'lib']);
    print('Code successfully formatted');
  } catch (e) {
    print("Unable to run 'flutter format lib'");
  }
}

class Translation {
  String msgid;
  String msgstr;
  Translation(this.msgid, this.msgstr);
}
